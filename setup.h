/**
 * setup.h: Setup entry point for creating/selecting profiles
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: setup.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __SETUP_H
#define __SETUP_H

#include <vdr/plugin.h>

class cSetupLastfm : public cMenuSetupPage
{
public:
	cSetupLastfm(void);
protected:
	virtual void Store(void);
	virtual eOSState ProcessKey(eKeys Key);
private:
	void BuildItems(void);
	char newProfile[ 512 ];
	cMenuEditStrItem * m_newentryitem;
};

#endif // __SETUP_H
