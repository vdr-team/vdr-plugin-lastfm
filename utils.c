/**
 * utils.c: Class delivers some static helper functions
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: utils.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <string.h>     // strdup()
#include <map>          // html2text
#include <stdlib.h>     // free()
#include <vdr/plugin.h> // tr()
#include "utils.h"
#include "md5.h"

// source: http://www.cplusplus.com/reference/stl/list/sort.html
// comparison, not case sensitive.
bool cUtils::compare_nocase(const std::string x, const std::string y)
{
	// the following code lines have been copied from: <http://souptonuts.sourceforge.net/code/list2.cc.html>

	std::string::const_iterator p = x.begin();
	std::string::const_iterator q = y.begin();

	while (p != x.end() && q != y.end() && toupper(*p) == toupper(*q) ) {
		++p;
		++q;
	}
	if (p == x.end() )
		return q != y.end();
	if (q == y.end() )
		return false;
	return toupper(*p) < toupper(*q);
}


std::string cUtils::rfc1738_encode(const std::string & src)
{
	static char hex[] = "0123456789ABCDEF";
	std::string dst;

	for (size_t i = 0; i < src.size(); i++) {
		if (isalnum(src[i]) ) {
			dst += src[i];
		} else if (src[i] == ' ') {
			dst += '+';
		} else {
			unsigned char c = static_cast<unsigned char>(src[i]);
			dst += '%';
			dst += hex[c / 16];
			dst += hex[c % 16];
		}
	}

	return dst;
} // rfc1738_encode


// copied from vdr-epgsearch written by C. Wieninger:
std::string cUtils::MD5(const std::string & input)
{
	char * szInput = strdup(input.c_str());

	if (!szInput) return "";
	char * szRes = MD5String(szInput);
	free(szInput);
	std::string res = szRes;
	free(szRes);
	return res;
}


const char * cUtils::GetFileNameChars_lastfm()
{
	return trNOOP("FileNameChars$ abcdefghijklmnopqrstuvwxyz0123456789-_.,#~\\^$[]|()*+?{}/:%@&"); // we need the underscore
}


int cUtils::html2text(std::string & output, const std::string input)
{
	std::string html_input = input;

	std::map<std::string, std::string> html_to_replace;
	std::map<std::string, std::string>::iterator el;

	std::string text_output;
	std::string::size_type pos = 0, loc_end = 0;

	html_to_replace[ "<br>" ] = "\n";
	html_to_replace[ "&nbsp;" ] = " ";
	html_to_replace[ "</a>" ] = "";
	html_to_replace[ "/>" ] = "";
	html_to_replace[ "<br" ] = "";
	html_to_replace["</span>"] = "";
	html_to_replace["</strong>"] = "";

	//HREF-links entfernen: (inklusive playerDetails.asp=098433)
	pos = 0;
	while ( (html_input.find("<a", pos) != std::string::npos ) &&
	       pos     < html_input.length() ) {
		pos = html_input.find("<a", pos);
		loc_end = html_input.find(">", pos);
		html_input.erase(pos, loc_end - pos + 1);
	}


	//HREF-links entfernen mit Grossbuchstaben <A HREF=
	pos = 0;
	while ( (html_input.find("<A", pos) != std::string::npos ) &&
	       pos     < html_input.length() ) {
		pos = html_input.find("<A", pos);
		loc_end = html_input.find(">", pos);
		html_input.erase(pos, loc_end - pos + 1);
	}

	// <span title=...>
	pos = 0;
	while ( (html_input.find("<span", pos) != std::string::npos ) &&
	       pos     < html_input.length() ) {
		pos = html_input.find("<span", pos);
		loc_end = html_input.find(">", pos);
		html_input.erase(pos, loc_end - pos + 1);
	}

	// <strong title=...>
	pos = 0;
	while ( (html_input.find("<strong", pos) != std::string::npos ) &&
	       pos     < html_input.length() ) {
		pos = html_input.find("<strong", pos);
		loc_end = html_input.find(">", pos);
		html_input.erase(pos, loc_end - pos + 1);
	}

	//HTML-Code ersetzen:
	for (el = html_to_replace.begin(); el != html_to_replace.end(); ++el) {
		pos = 0;
		while ( (html_input.find(el->first, pos) != std::string::npos ) && pos
		       < html_input.length() ) {
			pos = html_input.find(el->first, pos);
			html_input.replace(pos, el->first.size(), el->second);
		}
	}

	output = html_input;

	return 0;
}


