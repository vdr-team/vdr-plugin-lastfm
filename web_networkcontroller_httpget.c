/**
 * web_networkcontroller_httpget.c: HTTPGet
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httpget.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <sstream>
#include "web_networkcontroller_httpget.h"
#include "config.h" // PLUGIN_NAME PLUGIN_VERSION

// My_HttpGetSocket is a derivation of HttpGetSocket; in the original class,
// g-zip is supported. so compressed data would be received that we don't
// support by now.

My_Web_HttpGetSocket::My_Web_HttpGetSocket(ISocketHandler & h,
					   const std::string & url_in, const std::string & to_file) :
	HttpGetSocket(h, url_in)
{
}


// with that method, we can force the server to send us no gzipped data:
void My_Web_HttpGetSocket::OnConnect(void)
{
	SetMethod("GET");
	SetHttpVersion("HTTP/1.1");
	std::stringstream buff_stream;
	buff_stream << PLUGIN_NAME << "/"
		    << PLUGIN_VERSION;
	std::string buff;
	buff = buff_stream.str();

	AddResponseHeader("User-agent", buff);
	AddResponseHeader("Host", GetUrlHost() );

	SendRequest();
}


