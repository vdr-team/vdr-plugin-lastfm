/*
 * audio_filter.c:  The audio filter interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-06-30 19:45:30  mf $
 */
/*!
 * \file vdr_sound.c
 * \brief Sound manipulation classes for a VDR media plugin (muggle)
 *
 * \version $Revision: 1.2 $
 * \date    $Date$
 * \author  Ralf Klueber, Lars von Wedel, Andreas Kellner
 * \author  Responsible author: $Author$
 *
 * $Id$
 *
 * Adapted from
 * MP3/MPlayer plugin to VDR (C++)
 * (C) 2001,2002 Stefan Huelswitt <huels@iname.com>
 */

#include "config.h"
#include "logdefs.h"
#include "audio_filter.h"


// --- mgScale ----------------------------------------------------------------

mgScale::mgScale() {
#ifdef DEBUG
	clipped_samples = 0;
	peak_clipping = peak_sample = 0;
#endif
	memset (&leftD, 0, sizeof (leftD));
	memset (&rightD, 0, sizeof (rightD));
}

void
mgScale::Stats (void) {
#ifdef DEBUG
	LOGDGB("vdr-lastfm: scale stats clipped=%ld peak_clip=%f peak=%f\n",
		clipped_samples, mad_f_todouble (peak_clipping),
		mad_f_todouble (peak_sample));
#endif
}

// gather signal statistics while clipping
mad_fixed_t mgScale::Clip (mad_fixed_t sample, bool stats) {
#ifndef DEBUG
	if (sample > MAX)
		sample = MAX;
	if (sample < MIN)
		sample = MIN;
#else
	if (!stats) {
		if (sample > MAX)
			sample = MAX;
		if (sample < MIN)
			sample = MIN;
	}
	else {
		if (sample >= peak_sample) {
			if (sample > MAX) {
				++clipped_samples;
				if (sample - MAX > peak_clipping)
					peak_clipping = sample - MAX;
				sample = MAX;
			}
			peak_sample = sample;
		}
		else if (sample < -peak_sample) {
			if (sample < MIN) {
				++clipped_samples;
				if (MIN - sample > peak_clipping)
					peak_clipping = MIN - sample;
				sample = MIN;
			}
			peak_sample = -sample;
		}
	}
#endif
	return sample;
}

// generic linear sample quantize routine
signed long
mgScale::LinearRound (mad_fixed_t sample) {
	// round
	sample += (1L << (MAD_F_FRACBITS - OUT_BITS));
	// clip
	sample = Clip (sample);
	// quantize and scale
	return sample >> (MAD_F_FRACBITS + 1 - OUT_BITS);
}

// 32-bit pseudo-random number generator
unsigned long
mgScale::Prng (unsigned long state) {
	return (state * 0x0019660dL + 0x3c6ef35fL) & 0xffffffffL;
}

// generic linear sample quantize and dither routine
signed long
mgScale::LinearDither (mad_fixed_t sample, struct dither *dither) {
	unsigned int scalebits;
	mad_fixed_t output, mask, random;

	// noise shape
	sample += dither->error[0] - dither->error[1] + dither->error[2];
	dither->error[2] = dither->error[1];
	dither->error[1] = dither->error[0] / 2;
	// bias
	output = sample + (1L << (MAD_F_FRACBITS + 1 - OUT_BITS - 1));
	scalebits = MAD_F_FRACBITS + 1 - OUT_BITS;
	mask = (1L << scalebits) - 1;
	// dither
	random = Prng (dither->random);
	output += (random & mask) - (dither->random & mask);
	dither->random = random;
	// clip
	output = Clip (output);
	sample = Clip (sample, false);
	// quantize
	output &= ~mask;
	// error feedback
	dither->error[0] = sample - output;
	// scale
	return output >> scalebits;
}

// write a block of signed 16-bit big-endian PCM samples
unsigned int
mgScale::ScaleBlock (unsigned char *data, unsigned int size,
unsigned int &nsamples, const mad_fixed_t * &left,
const mad_fixed_t * &right, eAudioMode mode) {
	signed int sample;
	unsigned int len, res;

	len = size / OUT_FACT;
	res = size;
	if (len > nsamples) {
		len = nsamples;
		res = len * OUT_FACT;
	}
	nsamples -= len;

	if (right) {
								 // stereo
		switch (mode) {
			case amRound:
				while (len--) {
					sample = LinearRound (*left++);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
					sample = LinearRound (*right++);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
				}
				break;
			case amDither:
				while (len--) {
					sample = LinearDither (*left++, &leftD);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
					sample = LinearDither (*right++, &rightD);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
				}
				break;
		}
	}
	else {
								 // mono, duplicate left channel
		switch (mode) {
			case amRound:
				while (len--) {
					sample = LinearRound (*left++);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
					*data++ = sample >> 8;
					*data++ = sample >> 0;
				}
				break;
			case amDither:
				while (len--) {
					sample = LinearDither (*left++, &leftD);
					*data++ = sample >> 8;
					*data++ = sample >> 0;
					*data++ = sample >> 8;
					*data++ = sample >> 0;
				}
				break;
		}
	}
	return res;
}

// --- mgResample ------------------------------------------------------------

// The resample code has been adapted from the madplay project
// (resample.c) found in the libmad distribution


bool mgResample::SetInputRate (unsigned int oldrate, unsigned int newrate) {
	if (oldrate < 8000 || oldrate > newrate * 6) {
								 // out of range
#ifdef LASTFM_DEBUG 
		LOGDBG("WARNING: samplerate %d out of range 8000-%d\n", oldrate, newrate * 6);
#endif
		return 0;
	}
	ratio = mad_f_tofixed ((double) oldrate / (double) newrate);
	step = 0;
	last = 0;
#ifdef LASTFM_DEBUG
	static mad_fixed_t
		oldratio = 0;
	if (oldratio != ratio) {
		printf ("mad: new resample ratio %f (from %d kHz to %d kHz)\n",
			mad_f_todouble (ratio), oldrate, newrate);
		oldratio = ratio;
	}
#endif
	return ratio != MAD_F_ONE;
}

unsigned int
mgResample::ResampleBlock (unsigned int nsamples, const mad_fixed_t * old) {
	// This resampling algorithm is based on a linear interpolation, which is
	// not at all the best sounding but is relatively fast and efficient.
	//
	// A better algorithm would be one that implements a bandlimited
	// interpolation.

	mad_fixed_t *nsam = resampled;
	const mad_fixed_t *end = old + nsamples;
	const mad_fixed_t *begin = nsam;

	if (step < 0) {
		step = mad_f_fracpart (-step);

		while (step < MAD_F_ONE) {
			*nsam++ = step ? last + mad_f_mul (*old - last, step) : last;
			step += ratio;
			if (((step + 0x00000080L) & 0x0fffff00L) == 0)
				step = (step + 0x00000080L) & ~0x0fffffffL;
		}
		step -= MAD_F_ONE;
	}

	while (end - old > 1 + mad_f_intpart (step)) {
		old += mad_f_intpart (step);
		step = mad_f_fracpart (step);
		*nsam++ = step ? *old + mad_f_mul (old[1] - old[0], step) : *old;
		step += ratio;
		if (((step + 0x00000080L) & 0x0fffff00L) == 0)
			step = (step + 0x00000080L) & ~0x0fffffffL;
	}

	if (end - old == 1 + mad_f_intpart (step)) {
		last = end[-1];
		step = -step;
	}
	else
		step -= mad_f_fromint (end - old);

	return nsam - begin;
}

