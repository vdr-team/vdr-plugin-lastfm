/*
 * audio_decoder_mp3_http.h: The mp3 decoder via http
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-13 09:30:51  mf $
 */

#ifndef __AUDIO_DECODER_MP3_HTTP_H
#define __AUDIO_DECODER_MP3_HTTP_H

#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <Sockets/SocketHandler.h>
#include <Sockets/HttpGetSocket.h>
#include "audio_buffer.h"
#include "audio_decoder_mp3.h"


class cHttpMp3DecoderSocket : public HttpGetSocket
{
public:
	cHttpMp3DecoderSocket(ISocketHandler &);
	~cHttpMp3DecoderSocket();
	virtual uint32_t GetData(uint8_t * data, const uint32_t count);
private:
	virtual void OnHeader(const std::string&,const std::string&);
	virtual void OnRead(void);
	virtual std::string MyUseragent(void);
	cSimpleRingBuffer * mo_NETBuffer;
};


class cHttpMp3Decoder : public cMp3Decoder, public cThread
{
protected:
	virtual void Action(void);
public:
	cHttpMp3Decoder(void);
	~cHttpMp3Decoder();
	virtual uint8_t IsConnected(void);
	virtual uint8_t Open(std::string url);
private:
	virtual uint32_t GetDataFromSource(uint8_t * data, const uint32_t count);
	SocketHandler h; /** SocketHandler must be initialized before Socket. */
	cHttpMp3DecoderSocket p;
	uint8_t m_active;
};

#endif // __AUDIO_DECODER_MP3_HTTP_H
