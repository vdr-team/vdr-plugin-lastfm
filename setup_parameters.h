/**
 * setup_parameters.h: Setup submenu for changing user parameters
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-15 17:51:40  mf $
 */


#ifndef __SETUP_PARAMETERS_H
#define __SETUP_PARAMETERS_H

#include <string>
#include <list> // FavouriteList
#include "setup.h"

class cSetupParameters : public cSetupLastfm
{
public:
	cSetupParameters(const std::string profilename);
private:
	void BuildItems(void);
private:
	int newScrobblemode;
	int newReceivermode;
	int newAlbumcovermode;
	char newUsername[ 32 ];
	char newPassphrase[ 32 ];
	int newOsd_Autostartmode;
	void BuildOsdItems(void);
	char newFavourite[ 512 ];
	int newOsd_Mode;
	int newOsd_Refreshinterval;
	char * m_array_Receivermode[ 2 ];
	char * m_array_Osd_Mode[ 2 ];
	int newPlayer_Buffertime;
protected:
	virtual eOSState ProcessKey(eKeys Key);
	virtual void Store(void);
private:
	std::string m_profilename;
	std::list<std::string> m_FavouriteList;
};

#endif //__SETUP_PARAMETERS_H
