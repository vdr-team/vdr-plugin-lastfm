/*
 * audio_player.c: The audio player and control interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-17 10:17:31  mf $
 */

#include <sstream> // cWebServiceConnectorScrobbler::Submission
#include <limits.h>
#include <time.h>
#include <vdr/status.h>
#include "audio_player.h"
#include "config.h"
#include "audio_control.h"
#include <vdr/tools.h>
#include "logdefs.h"
#include "audio_filter.h"


extern std::map<std::string, std::string> ParameterMap;

cAudioPlayer::cAudioPlayer(void)
:cPlayer(pmAudioOnly)
{
	SetDescription(PLUGIN_NAME "-AudioPlayer");
	m_ringbuffer = new cRingBufferFrame (PLAYER_BUFSIZE);
	m_playMode = pmPlay;
	httpmp3decoder = 0;
	int ret = 0;


	switch(ret = Initialize()) {
		case 1:
			Skins.Message(mtError, tr("Handshake unsuccessful!; no such user -> means username or password_md5 are wrong"), 3);
			Quit();
			break;
		case 2:
			Skins.Message(mtError, tr("Handshake unsuccessful! Network failure"), 3);
			Quit();
			break;
		case 3:
			Skins.Message(mtError, tr("Received no data"), 3);
			Quit();
			break;
		case 4:
			Skins.Message(mtError, tr("No webservices available; cannot love, ban, etc."), 3);
			// Quit(); do not quit, not critical
			break;
	}

	Start();
}


cAudioPlayer::~cAudioPlayer(void)
{
	Quit();

	if (httpmp3decoder)
		DELETENULL(httpmp3decoder);

	if (m_ringbuffer)
		DELETENULL(m_ringbuffer);
}


void cAudioPlayer::Action(void)
{
	cFrame *m_rframe=0, *m_pframe=0;
	enum eState
	{
		msStart, msStop,
		msDecode, msNormalize,
		msResample, msOutput,
		msError, msEof, msWait
	};
	eState m_state;
	struct mad_pcm pcm;

	unsigned int nsamples[2];
	const mad_fixed_t *data[2];



	struct LPCMFrame
	{
		unsigned char PES[HDR_SIZE];
		unsigned char LPCM[LPCM_SIZE];
		unsigned char Data[MAX_FRAMESIZE - HDR_SIZE - LPCM_SIZE];
	};
	struct LPCMFrame lpcmFrame;
	mgResample resample[2];
	int only48khz = 1;
	uint32_t dvbSampleRate = 48000;

	mgScale scale;

	int pc = 0;
	const unsigned char *p = 0;


	dsyslog ("vdr-lastfm: player thread started (pid=%d)", getpid ());

	memset (&lpcmFrame, 0, sizeof (lpcmFrame));
	lpcmFrame.PES[2] = 0x01;
	lpcmFrame.PES[3] = 0xbd;
	lpcmFrame.PES[6] = 0x87;
	lpcmFrame.LPCM[0] = 0xa0;	 // substream ID
	lpcmFrame.LPCM[1] = 0xff;
	lpcmFrame.LPCM[2] = 0;
	lpcmFrame.LPCM[3] = 4;
	lpcmFrame.LPCM[5] = 0x01;
	lpcmFrame.LPCM[6] = 0x80;

	dvbSampleRate = 48000;
	m_state = msDecode;

	cPoller poll;

	cDevice::PrimaryDevice()->SetCurrentAudioTrack(ttAudio);

	while (Running() && m_playMode!=pmStopped) {
		if (DevicePoll(poll, POLLTIMEOUT)) {
			LOCK_THREAD;
			if (!httpmp3decoder) {
				Next();
			} else  { // Decode, resample and put data on Dvbdevice, copied from project vdr-muggle:
				if (!m_rframe) {
					switch (m_state) {
						case msDecode:
							{
#ifdef LASTFM_DEBUG_AUDIO
								LOGDBG("msDecode");
#endif
								// Get decoded pcm data from mp3decoder:
								memcpy(&pcm, httpmp3decoder->GetData(), sizeof(pcm));

								uint16_t len=pcm.length;
								if (!len || httpmp3decoder->Status()==0) {
#ifdef LASTFM_DEBUG_AUDIO
									LOGDBG("cAudioPlayer::Action: if (!len)\r\n");
#endif
									if (!httpmp3decoder->IsConnected() && !len) {
#ifdef LASTFM_DEBUG_AUDIO
										LOGDBG("cAudioPlayer::Action: if (!httpmp3decoder->IsConnected())\r\n");
#endif
										Next();
									} else {
										static int cnt_bufferunderruns = 0;

										if (cnt_bufferunderruns > 2) {
											cnt_bufferunderruns = 0;
											Next();
										}
										cnt_bufferunderruns++;

										time_t time_lastconnloss = time(0);
										while (time(0) - time_lastconnloss < atoi(ParameterMap[PARAMETERMAP_PLAYER_BUFFERTIME].c_str())) {
#ifdef LASTFM_DEBUG_AUDIO
											LOGDBG("cAudioPlayer::Action: buffering\r\n");
#endif
											cCondWait::SleepMs(3);
										}
										Clear();
									}
								} else if (len) {
									m_state = msResample;
								}
							}
							break;
						case msResample:
							{
#ifdef LASTFM_DEBUG_AUDIO
								LOGDBG("msResample");
#endif

								uint32_t pcm_samplerate = pcm.samplerate;
#ifdef LASTFM_DEBUG_AUDIO
								{
									static unsigned int oldrate = 0;
									if (oldrate != pcm_samplerate) {
										LOGDBG("mgPCMPlayer::Action: new input sample rate %d",pcm_samplerate);
										oldrate = pcm_samplerate;
									}
								}
#endif
								nsamples[0] = nsamples[1] = pcm.length;
								data[0] = pcm.samples[0];
								data[1] = pcm.channels > 1 ? pcm.samples[1] : 0;

								lpcmFrame.LPCM[5] &= 0xcf;
								dvbSampleRate = 48000;

								if (!only48khz) {
									switch (pcm_samplerate) {
										// If one of the supported frequencies, do it without resampling.
										case 96000:
											{	 // Select a "even" upsampling frequency if possible, too.
												lpcmFrame.LPCM[5] |= 1 << 4;
												dvbSampleRate = 96000;
											}
											break;

											//case 48000: // this is already the default ...
											//  lpcmFrame.LPCM[5]|=0<<4;
											//  dvbSampleRate=48000;
											//  break;
										case 11025:
										case 22050:
										case 44100:
											{
												// lpcmFrame.LPCM[5] |= 2 << 4;
												lpcmFrame.LPCM[5] |= 0x20;
												dvbSampleRate = 44100;
											}
											break;
										case 8000:
										case 16000:
										case 32000:
											{
												// lpcmFrame.LPCM[5] |= 3 << 4;
												lpcmFrame.LPCM[5] |= 0x30;
												dvbSampleRate = 32000;
											}
											break;
									}
								}

								if (dvbSampleRate != pcm_samplerate) {
									if (resample[0].
											SetInputRate (pcm_samplerate, dvbSampleRate)) {
										nsamples[0] =
											resample[0].ResampleBlock (nsamples[0], data[0]);
										data[0] = resample[0].Resampled ();
									}
									if (data[1]
											&& resample[1].SetInputRate (pcm_samplerate,
												dvbSampleRate)) {
										nsamples[1] =
											resample[1].ResampleBlock (nsamples[1], data[1]);
										data[1] = resample[1].Resampled ();
									}
								}
								m_state = msOutput;
							}
							break;
						case msOutput:
							{
#ifdef LASTFM_DEBUG_AUDIO
								LOGDBG("msOutput");
#endif
								int AudioMode=0;

								if (nsamples[0] > 0) {
									unsigned int outlen =
										scale.ScaleBlock ( lpcmFrame.Data,
												sizeof (lpcmFrame.Data),
												nsamples[0],
												data[0], data[1],
												AudioMode ?
												amDither : amRound );
									if (outlen) {
										outlen += sizeof (lpcmFrame.LPCM) + LEN_CORR;
										lpcmFrame.PES[4] = outlen >> 8;
										lpcmFrame.PES[5] = outlen;

										// lPCM has 600 fps which is 80 samples at 48kHz per channel
										// Frame size = (sample_rate * quantization * channels)/4800
										size_t frame_size = 2 * (dvbSampleRate*16)/4800;

										lpcmFrame.LPCM[1] = (outlen - LPCM_SIZE)/frame_size;
										m_rframe = new cFrame ((unsigned char *) &lpcmFrame,
												outlen +
												sizeof (lpcmFrame.PES) -
												LEN_CORR);
									}
								}
								else {
									m_state = msDecode;
								}
							}
							break;
						default:
							break;
					}
				}

				if (m_rframe && m_ringbuffer->Put(m_rframe)) {
					m_rframe = 0;
				}

				if (!m_pframe) {
					m_pframe = m_ringbuffer->Get ();
					if (m_pframe) {
						p = m_pframe->Data ();
						pc = m_pframe->Count ();
					}
				}

				if (m_pframe) {
#ifdef DEBUGPES
					fwrite( (void *)p, pc, sizeof( char ), peslog );
#endif
					int w = PlayPes (p, pc);
					if (w > 0) {
						p += w;
						pc -= w;

						if (pc <= 0) {
							m_ringbuffer->Drop (m_pframe);
							m_pframe = 0;
						}
					}
					else if (w < 0 && FATALERRNO) {
						LOG_ERROR;
						break;
					}
				}

			} // Decode, resample and put data on Dvbdevice
		} // DevicePoll
	} // while (Running())
}


void cAudioPlayer::SetPlayMode(ePlayMode mode)
{
	mo_playerMutex.Lock();
	m_playMode = mode;
	mo_playerCond.Broadcast();
	mo_playerMutex.Unlock();
}


void cAudioPlayer::Clear(void)
{
	LOCK_THREAD;
	DeviceClear();
	m_ringbuffer->Clear(); /** After clearing device, we clear ringbuffer. */
}


void cAudioPlayer::Stop(void)
{
	LOCK_THREAD;
	// SetPlayMode() must be called after Reset()
	Clear();
	SetPlayMode(pmStopped);
}


void cAudioPlayer::Next(void)
{
	LOCK_THREAD;

	Clear();
	DevicePlay();

	// Get new songurl:
	std::string url(GetNextsongurl());

	if (httpmp3decoder)
		DELETENULL(httpmp3decoder);

	if (url.compare("") == 0)
		return;

	httpmp3decoder = new cHttpMp3Decoder();
	httpmp3decoder->Open(url);
}


bool cAudioPlayer::GetIndex(int & Current, int & Total, bool SnapToIFrame)
{
	if (httpmp3decoder)
		return httpmp3decoder->GetIndex(Current, Total);

	return false;
}

void cAudioPlayer::Quit(void) {
	Cancel(MAXIMUM_THREADKILLTIME);
	Stop();
	Detach();
	cControl::Shutdown();
}

