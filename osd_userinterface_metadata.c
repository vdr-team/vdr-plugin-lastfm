/**
 * osd_userinterface_metadata.c: Presents submenus with metadata items
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-15 18:01:52  mf $
 */


#include "osd_userinterface_metadata.h"
#include <vdr/tools.h>
#include "logdefs.h"
#include "web_networkcontroller.h"
#include "osd_userinterface_data_songLyrics.h"
#include "osd_userinterface_data_artistMetadata.h"
#include "osd_userinterface_data_recenttracks.h"
#include "osd_userinterface_data_getlovedtracks.h"


cUserInterface_Metadata::cUserInterface_Metadata(void)
	: cOsdMenu(tr("metadata"), 20)
{
	BuildItems();  // OSD-Elemente aufbauen
}


void cUserInterface_Metadata::BuildItems(void)
{
	Add(new cOsdItem(hk(tr("songLyrics...") ) ) );
	Add(new cOsdItem(hk(tr("artistMetadata...") ) ) );
	Add(new cOsdItem(hk(tr("recentLovedTracks..." ) ) ) );
	Add(new cOsdItem(hk(tr("recentTracks..." ) ) ) );
	Display();
}


eOSState cUserInterface_Metadata::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);

	if (state == osUnknown) {
		switch (Key) {
		case kOk:
#ifdef LASTFM_DEBUG
			LOGDBG("eOSState cUserInterface_Metadata::ProcessKey: switch( Key ): %d\r\n", Key);
#endif
			if (!strcmp(Get(Current())->Text(), tr("songLyrics...") ) ) {
				AddSubMenu(new cUserInterface_Data_songLyrics() );
			} else if (!strcmp(Get(Current())->Text(), tr("artistMetadata..."))) {
				AddSubMenu(new cUserInterface_Data_artistMetadata());
			} else if (!strcmp(Get(Current())->Text(), tr("recentLovedTracks..."))) {
				AddSubMenu(new cUserInterface_Data_getLovedTracks());
			} else if (!strcmp(Get(Current())->Text(), tr("recentTracks..."))) {
				AddSubMenu(new cUserInterface_Data_recentTracks());
			}

		default:
			state = osContinue;
			break;
		}
	}
	return state;
}


