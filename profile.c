/**
 * profile.c: Profile
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2010-11-20 15:25:07  mf $
 */

#include <sstream>
#include "profile.h"
#include <vdr/tools.h>
#include "logdefs.h"
#include "audio_control.h"
#include "playlist.h"
#include "web_serviceconnector.h"
#include "web_serviceconnector_webservices.h"
#include "utils.h"                                      // md5

extern std::map<std::string, std::string> ParameterMap; // username, passphrase_md5
extern std::map<std::string, std::string> MetadataMap;  // session, track, album...

cProfile::cProfile(void)
{
}


cProfile::~cProfile()
{
	MetadataMap.clear();
}


int cProfile::Initialize(void)
{
	int ret = 0;
	std::string username, passphrase_md5;

	m_running = 0;

	username = ParameterMap[PARAMETERMAP_USERNAME];
	passphrase_md5 = ParameterMap[PARAMETERMAP_PASSPHRASE_MD5];

	ret = cWebServiceConnector::Handshake(username, passphrase_md5, m_session, m_subscriber);
	if (ret == 0) {
		MetadataMap["session"] = m_session; // Globalize session, so OSD submenus can use it easily.
	}
	else {
		return 1;
	}

	ret = cWebServiceConnectorWebservices::auth.getMobileSession(username, passphrase_md5,
								     m_session_websvcs, m_authtoken);
	if (ret) {
		return 4; // We are losing webservice functions, this is not crititcal.
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cProfile::Initialize: %d\r\n", ret);
#endif
	return ret;
}


int cProfile::FeedPlaylist(void)
{
	static time_t time_Playlist = time(0);
	std::queue<std::map<long, std::map<std::string, std::string> > > trackList;

	if (time_Playlist + GETPLAYLIST_DELAY < time(0))
	{
		cWebServiceConnector::GetPlaylist(m_session, trackList);
		time_Playlist = time(0);
	}

	m_trackList = trackList;

	return 0;
}


std::string cProfile::GetNextsongurl(void)
{
	std::string nexttrack;
	static time_t time_trackstart = time(0);

	if (!m_trackList.empty()) {
		if (!ParameterMap[PARAMETERMAP_SCROBBLEMODE].compare("1")) {
			Submit(0, (time(0) - time_trackstart));
		}
		// delete previous first entry of queue
		m_trackList.pop();
	}

	if (m_trackList.empty()) {
		FeedPlaylist();
	}

	if (!m_trackList.empty()) {
		nexttrack = m_trackList.front().begin()->second["track_location"]; // present the next track as the first in the queue:
		if (!ParameterMap[PARAMETERMAP_SCROBBLEMODE].compare("1")) {
			NowPlaying();
		}
		time_trackstart = time(0);

		// Store metadata to global Map (this way, osd submenus can access further data related to playing music):
		MetadataMap["track_creator"] = m_trackList.front().begin()->second["track_creator"];
		MetadataMap["track_title"] = m_trackList.front().begin()->second["track_title"];

	}

	return nexttrack;
}


int cProfile::Submit(int i, int pos)
{
	if (!m_trackList.empty()) {

		std::stringstream sstream_tmp;
		std::string api_signature;
		int position = 0;
		int total = 0;
		std::string rating;
		std::stringstream tracksource;

		sstream_tmp.str("");
		sstream_tmp.clear();
		sstream_tmp << m_trackList.front().begin()->second["track_duration"];
		sstream_tmp >> total;

		position = pos;

		if (i == 1) {
			rating = "L";

			sstream_tmp.str("");
			sstream_tmp.clear();
			sstream_tmp << "api_key" << APIKEY
				    << "artist" << m_trackList.front().begin()->second["track_creator"]
				    << "methodtrack.love"
				    << "sk" << m_session_websvcs
				    << "track" << m_trackList.front().begin()->second["track_title"]
				    << APISECRET;
#ifdef LASTFM_DEBUG_WEBSERVICE
			LOGDBG("api_signature: %s\r\n", sstream_tmp.str().c_str());
#endif
			api_signature = cUtils::MD5(sstream_tmp.str());
			cWebServiceConnectorWebservices::track.love(m_trackList.front().begin()->second["track_title"],
								    m_trackList.front().begin()->second["track_creator"],
								    APIKEY,
								    api_signature,
								    m_session_websvcs);
		} else if (i == 2) {
			rating = "B";

			sstream_tmp.str("");
			sstream_tmp.clear();
			sstream_tmp << "api_key" << APIKEY
				    << "artist" << m_trackList.front().begin()->second["track_creator"]
				    << "methodtrack.ban"
				    << "sk" << m_session_websvcs
				    << "track" << m_trackList.front().begin()->second["track_title"]
				    << APISECRET;
			api_signature = cUtils::MD5(sstream_tmp.str());
			cWebServiceConnectorWebservices::track.ban(m_trackList.front().begin()->second["track_title"],
								   m_trackList.front().begin()->second["track_creator"],
								   APIKEY,
								   api_signature,
								   m_session_websvcs);
		} else if (i == 0) {
			std::stringstream timestamp_track_start_sstream;
			std::stringstream timestamp_track_start_sstream2;
			std::string timestamp_track_start_str;
			int32_t timestamp_track_start;
			int32_t track_duration;

			timestamp_track_start_sstream << m_trackList.front().begin()->second["track_duration"];
			timestamp_track_start_sstream >> track_duration;
			timestamp_track_start = time(0);
			timestamp_track_start -=track_duration;
			timestamp_track_start_sstream.str("");
			timestamp_track_start_sstream.clear();
			timestamp_track_start_sstream2 << timestamp_track_start;
			timestamp_track_start_sstream2 >> timestamp_track_start_str;

			cWebServiceConnectorWebservices::track.scrobble(m_trackList.front().begin()->second["track_title"],
								   timestamp_track_start_str,
								   m_trackList.front().begin()->second["track_creator"],
								   m_session_websvcs);
		}
	}

	return 0;
}


int cProfile::NowPlaying(void)
{
	if (!m_trackList.empty()) {
		cWebServiceConnectorWebservices::track.updateNowPlaying(m_trackList.front().begin()->second["track_title"],
									m_trackList.front().begin()->second["track_creator"],
									m_session_websvcs);
	}

	return 0;
}
