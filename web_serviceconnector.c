/*
 * web_serviceconnector.c: Desc
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-12 14:29:53  mf $
 */

#include "web_serviceconnector.h"
#include <sstream>
#include "config.h"
#include "web_networkcontroller.h"
#include <vdr/tools.h>
#include "logdefs.h"
#include "ext/xmlParser/xmlParser.h"    // in order to parse metadata
#include "utils.h"                      // rfc1738_encode

/**
 *
 * Handshake
 *
 * @param: username
 * @param: password
 *
 * @return: 0==noerror; 2==networkfailure; 3=="no such user"
 */
int cWebServiceConnector::Handshake(const std::string username, const std::string password_md5,
				    std::string & session, int & subscriber)
{
	int ret = 0;
	std::stringstream buf_ss;
	std::string document_str;

	std::stringstream url_stream;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH
		   << "/handshake.php?"
		   << "version=" << AUDIOSCROBBLER_CLIENTVERSION
		   << "&platform=linux"
		   << "&username=" << username
		   << "&passwordmd5=" << password_md5
		   << "&debug=0";

	cNetworkController NetworkController;
	if (!NetworkController.HttpGetRequest(document_str, url_stream.str())) {
		return 2;
	}
#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnector::Handshake:NetworkController.HttpGetRequest: %s\r\n", document_str.c_str());
#endif
	// Here, we parse the response for streamurl, session and subscriber state:
	size_t pos;
	if ((pos = document_str.find("session=")) != std::string::npos) {
		buf_ss << document_str.substr(pos + sizeof("session=") - 1);
		buf_ss >> session;
	}
	buf_ss.clear();
	if ((pos = document_str.find("subscriber=")) != std::string::npos) {

		buf_ss << document_str.substr(pos + sizeof("subscriber=") - 1);
		buf_ss >> subscriber;
	}
	buf_ss.clear();

	if (session.empty() || session.compare("FAILED") == 0)
		ret = 1;

	return ret;
}


int cWebServiceConnector::GetPlaylist(const std::string session,
				      std::queue<std::map<long, std::map<std::string, std::string> > > & trackList)
{

	/* from bmpx project (lastfm.cc):
	   std::string uri ((boost::format ("http://%s/%s/xspf.php?sk=%s&discovery=%d&desktop=1.3.0.58")
	   % m_session.base_url
	   % m_session.base_path
	   % m_session.session
	   % int (mcs->key_get<bool>("lastfm","discoverymode"))).str());
	 */

	std::string document_str;
	std::stringstream url_stream, sstream_tmp;

	std::map<std::string, std::string> postdata;

	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH << "/xspf.php"
		   << "?" << "sk=" << session
		   << "&" << "discovery=" << "0"
		   << "&" << "desktop=" << "0";

	if (!NetworkController.HttpPostRequest(document_str, url_stream.str(), postdata)) {
		return 2;
	}
#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnector::GetPlaylist:NetworkController.HttpGetRequest: %s\r\n", document_str.c_str());
#endif
	// erhaltene XML-Daten durchforsten:
	XMLNode xMainNode, xNode;
	XMLResults xe;

	xMainNode = XMLNode::parseString(document_str.c_str(), "", &xe);

	if (xe.error)
		return 3;

	int n = xMainNode.getChildNode("trackList").nChildNode("track");

	if (!n)
		return 3; // no tracklist found

	for (int i = 0; i < n; i++) {
		std::string track_location, track_lastfm_trackauth, track_creator,
			    track_title, track_duration_str, track_album, track_image;
		long track_id, track_duration_long;

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("id").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("id").getText());
			sstream_tmp >> track_id;
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("location").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("location").getText());
			track_location = sstream_tmp.str();
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("duration").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("duration").getText());
			sstream_tmp >> track_duration_long;
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("lastfm:trackauth").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("lastfm:trackauth").getText());
			track_lastfm_trackauth = sstream_tmp.str();
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("creator").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("creator").getText());
			track_creator = sstream_tmp.str();
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("title").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("title").getText());
			track_title = sstream_tmp.str();
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("album").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("album").getText());
			track_album = sstream_tmp.str();
		}

		if (xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("image").nElement()) {
			sstream_tmp.clear();
			sstream_tmp.str(xMainNode.getChildNode("trackList").getChildNode("track", i).getChildNode("image").getText());
			track_image = sstream_tmp.str();
		}
		// copy the gained values to the public cAudioPlaylist
		std::map<std::string, std::string> trackdata;
		std::map<long, std::map<std::string, std::string> >track;
		trackdata["track_location"] = track_location;

		// track duration (needs calculation):
		sstream_tmp.clear();
		sstream_tmp.str("");
		sstream_tmp << track_duration_long; //// / 1000;
		sstream_tmp >> track_duration_str;
		trackdata["track_duration"] = track_duration_str;

		trackdata["track_lastfm_trackauth"] = track_lastfm_trackauth;
		trackdata["track_creator"] = track_creator;
		trackdata["track_title"] = track_title;
		trackdata["track_album"] = track_album;
		trackdata["track_image"] = track_image;

		track[track_id] = trackdata;

		trackList.push(track);
	} // for (int i=0; i<n; i++)


	return 0;
}


int cWebServiceConnector::Changestation(const std::string session, const std::string lastfm_url)
{
	std::string buffer_HttpGetResponse, l_lastfm_url;
	std::stringstream url_stream;

	// while encoding, spaces will be replaced by a  " + "  and
	// lastfm-Server does not recognize it; but encoding seems not to be needed
	// at all.
	l_lastfm_url = cUtils::rfc1738_encode(lastfm_url);

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH << "/adjust.php?" << "session=" << session
		   << "&url=" << l_lastfm_url << "&debug=0";

	cNetworkController NetworkController;

	if (!NetworkController.HttpGetRequest(buffer_HttpGetResponse, url_stream.str() ) ) {
		return false;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnector::Changestation:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpGetResponse.c_str());
#endif
	// Pruefen, ob der Response Code OK ist
	/* Auszug aus offiziellem last.fm-Client:
	   // (last.fm-1.3.1.0/src/libLastFmTools/WebService/ChangeStationRequest.cpp

	   case 1:
	   setFailed( ChangeStation_NotEnoughContent,
	   tr( "Sorry, there is not enough content to play this station. Please choose a different one." ) );
	   break;

	   case 2:
	   setFailed( ChangeStation_TooFewGroupMembers,
	   tr( "This group does not have enough members for radio." ) );
	   break;

	   case 3:
	   setFailed( ChangeStation_TooFewFans,
	   tr( "This artist does not have enough fans for radio." ) );
	   break;

	   case 4:
	   setFailed( ChangeStation_Unavailable,
	   tr( "This item is not available for streaming." ) );
	   break;

	   case 5:
	   setFailed( ChangeStation_SubscribersOnly,
	   tr( "This station is available to subscribers only."
	   "<p>" "You can subscribe here: <a href='http://www.last.fm/subscribe/'>http://www.last.fm/subscribe/</a>" ) );
	   break;

	   case 6:
	   setFailed( ChangeStation_TooFewNeighbours,
	   tr( "There are not enough neighbours for this radio mode." ) );
	   break;

	   case 7:
	   setFailed( ChangeStation_StreamerOffline,
	   tr( "The streaming system is offline for maintenance, please try again later." ) );
	   break;

	   case 8:
	   setFailed( ChangeStation_InvalidSession,
	   tr( "The streaming system is offline for maintenance, please try again later." ) );
	   break;

	   default:
	   setFailed( ChangeStation_UnknownError,
	   tr( "Starting radio failed. Unknown error." ) );
	   break;
	 */

	//also  " ERROR - no such artist "  in Zeichenkette vorhanden/gefunden
	if (strstr(buffer_HttpGetResponse.c_str(), "ERROR - no such artist") != 0) {
		return 2;
	}

	if (strstr(buffer_HttpGetResponse.c_str(), "error=5") != 0) {
		return 5;
	}


	return 0;
}


