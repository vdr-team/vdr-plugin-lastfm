/**
 * setup_favourites.c: Setup submenu for creating/deleting favourite entries
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: setup_favourites.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <vdr/menuitems.h>
#include "setup_favourites.h"
#include "utils.h"      // store not case sensitive

cSetupFavourites::cSetupFavourites(std::list<std::string> & FavouriteList)
	: cOsdMenu(tr("Favourites"), 20)
	, m_FavouriteList(FavouriteList)
{
	strncpy(newfavourite, "", 511);

	// OSD-Elemente aufbauen
	BuildItems();
}


void cSetupFavourites::BuildItems(void)
{
	Clear();

	SetHelp(0, 0, trVDR("Button$Remove") );

	Add(new cMenuEditStrItem(trVDR("Button$New"),
				 newfavourite, sizeof(newfavourite), tr(cUtils::GetFileNameChars_lastfm() ) ) );

	for (std::list<std::string>::iterator it = m_FavouriteList.begin();
	     it != m_FavouriteList.end();
	     it++) {
		std::string it_string = *it;
		Add(new cOsdItem(it_string.c_str() ) );

		Display();
	}

}


eOSState cSetupFavourites::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);

	/** display SetHelp buttons according to the lists */
	if (Get(0) == Get(Current() ) )   /** first entry */
		SetHelp(0, 0, 0);
	else
		SetHelp(0, 0, trVDR("Button$Remove") );

	if (state == osUnknown) {
		switch (Key & ~k_Repeat) {

		case kOk:
			if (strcmp(newfavourite, "") ) {
				m_FavouriteList.push_back(newfavourite);
				// sort favouritelist
				// data will be sorted here, because of new entered items by user;
				// these items have not been stored in setup.conf, yet.
				m_FavouriteList.sort(cUtils::compare_nocase);
			}

			return osBack;

		case kYellow:
			if (!m_FavouriteList.empty() ) {
				m_FavouriteList.remove(Get(Current() )->Text() );
				Del(Current());           /// Im aktuellen OSD ebenfalls entfernen
			}
			Display();

			return osContinue;

		default:
			state = osContinue;
			break;
		}
	}
	return state;

}


