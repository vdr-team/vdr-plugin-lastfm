/**
 * serviceclient.c: demo plugin, that connects to vdr-lastfm, handshakes and
 * can submit music titles
 *
 * Copyright: See COPYING file that comes with this distribution
 *
 * Author: Matthias Feistel <hitman_47@users.sourceforge.net>, (C) 2007
 */
/*
 * svccli.c: Sample service client plugin
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: svccli.c 1.1 2005/08/21 10:44:29 kls Exp $
 */

#include <stdlib.h>
#include <vdr/interface.h>
#include <vdr/plugin.h>
#include <string>
#include <iostream>     // std::cout
#include "../service.h" // include service typedefs, struct


static const char * VERSION = "0.1.2";
static const char * DESCRIPTION = "Service demo client";
static const char * MAINMENUENTRY = "Service demo";

class cPluginSvcCli : public cPlugin
{
public:
	virtual const char * Version(void)
	{
		return VERSION;
	}


	virtual const char * Description(void)
	{
		return DESCRIPTION;
	}


	virtual const char * MainMenuEntry(void)
	{
		return MAINMENUENTRY;
	}


	virtual cOsdObject * MainMenuAction(void);
};


// --- cPluginSvcCli ----------------------------------------------------------

cOsdObject * cPluginSvcCli::MainMenuAction(void)
{
	cPlugin * p;
	SubmissionHandle_v1_0 sbp;

	p = cPluginManager::CallFirstService("lastfm_submission_v1_0", 0);

	if (p) {
		p->Service("lastfm_submission_v1_0", &sbp);

		std::string data_npurl, data_protourl, data_sessionid;

		sbp.HandshakeV12("username", "password_md5", data_npurl,
				 data_protourl, data_sessionid);

		sbp.Submission(data_protourl, data_sessionid, "artist", "track",
			       "secs", "source", 51, 240, "rating", "tracktime", "album",
			       "tracknumber", "mb_trackid");

		std::cout << "returned HANDSHAKEv12-URL: " << data_protourl << std::endl;
		std::cout << "returned Now-Playing-URL: " << data_npurl << std::endl;
		std::cout << "returned Session ID: " << data_sessionid << std::endl;

	}

	return 0;
}


VDRPLUGINCREATOR(cPluginSvcCli);   // Don't touch this!

/*
   s=<sessionID>
    The Session ID string as returned by the handshake. Required.
   a[0]=<artist>
    The artist name. Required.
   t[0]=<track>
    The track title. Required.
   i[0]=<time>
    The time the track started playing, in UNIX timestamp format (integer number of seconds since 00:00:00, January 1st 1970 UTC). This must be in the UTC time zone, and is required.
   o[0]=<source>
    The source of the track. Required, must be one of the following codes:

    P
	Chosen by the user
    R
	Non-personalised broadcast (e.g. Shoutcast, BBC Radio 1)
    E
	Personalised recommendation except Last.fm (e.g. Pandora, Launchcast)
    L
	Last.fm (any mode). In this case, the 5-digit Last.fm recommendation key must be appended to this source ID to prove the validity of the submission (for example, "o[0]=L1b48a").
    U
	Source unknown

   r[0]=<rating>
    The rating of the track. Empty if not applicable.

    L
	Love (on any mode if the user has manually loved the track)
    B
	Ban (only if source=L)
    S
	Skip (only if source=L)

    Note: Currently, a web-service must also be called to set love/ban status. We anticipate that this will be phased out soon, and the submission service will handle the whole process.
   l[0]=<secs>
    The length of the track in seconds. Required when the source is P, optional otherwise.
   b[0]=<album>
    The album title, or empty if not known.
   n[0]=<tracknumber>
    The position of the track on the album, or empty if not known.
   m[0]=<mb-trackid>
    The MusicBrainz Track ID, or empty if not known.
 */
