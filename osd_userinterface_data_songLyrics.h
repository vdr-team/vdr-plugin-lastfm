/**
 * osd_userinterface_data_songLyrics.h: Presents lyrics of the current song
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-13 22:04:22  mf $
 */

#ifndef __USERINTERFACE_DATA_SONGLYRICS_H
#define __USERINTERFACE_DATA_SONGLYRICS_H

#include <vdr/menu.h>

class cUserInterface_Data_songLyrics : public cMenuText
{
private:
	void BuildItems(void);
public:
	cUserInterface_Data_songLyrics(void);
};

#endif //__USERINTERFACE_DATA_SONGLYRICS_H
