/**
 * osd_userinterface_search.c: Search for artists and tags
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-05-08 20:00:06  mf $
 */

#include <vdr/remote.h>
#include "osd_userinterface_search.h"
#include <sstream>
#include <vdr/tools.h>
#include "logdefs.h"
#include "osd_userinterface_search_favourites.h"
#include "config.h"

extern std::map<std::string, std::string> MetadataMap;

cUserInterface_Search::cUserInterface_Search(void)
	: cOsdMenu(tr("search"), 20)
{
	strncpy(m_searchword, "", SEARCHWORD_MAXSIZE);
	BuildItems();  // OSD-Elemente aufbauen
}


void cUserInterface_Search::BuildItems(void)
{
	SetHelp(0, 0, 0, tr("Favourites...") );
	Add(new cMenuEditStrItem(tr("similarArtist"),
				 m_searchword, sizeof(m_searchword), tr(FileNameChars) ) );
	Add(new cMenuEditStrItem(tr("Tag"),
				 m_searchword, sizeof(m_searchword), tr(FileNameChars) ) );
	Add(new cMenuEditStrItem(tr("Top Fans Radio"),
				 m_searchword, sizeof(m_searchword), tr(FileNameChars) ) );
	Display();
}


eOSState cUserInterface_Search::ProcessKey(eKeys Key)
{
	std::string searchelement;
	std::stringstream sstream1;
	int rc = 0;

	eOSState state = cOsdMenu::ProcessKey(Key);

	if (state == osUnknown) {
		switch (Key) {
		case kOk:
			searchelement = Get(Current() )->Text();

			if (searchelement.find(tr("similarArtist") )
			    != std::string::npos) {
				sstream1.str("");
				sstream1.clear();
				sstream1 << "lastfm://artist/" << m_searchword << "/similarartists";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Search::ProcessKey: Changestation\r\n");
#endif
				rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					if (rc == 2) {
						Skins.Message(mtError, tr("No such artist!") );
					}
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");         // return to mainmenu
				}
			} else if (searchelement.find(tr("Tag") )
				   != std::string::npos) {
				sstream1.str("");
				sstream1.clear();
				sstream1 << "lastfm://globaltags/" << m_searchword;
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Search::ProcessKey: Changestation\r\n");
#endif
				rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					if (rc == 2) {
						Skins.Message(mtError, tr("No such tag!") );
					}
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");         // return to mainmenu
				}
			} else if (searchelement.find(tr("Top Fans Radio") )
				   != std::string::npos) {
				sstream1.str("");
				sstream1.clear();
				sstream1 << "lastfm://artist/" << m_searchword << "/fans";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Search::ProcessKey: Changestation\r\n");
#endif
				rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					if (rc == 2) {
						Skins.Message(mtError, tr("No such station!") );
					}
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");         // return to mainmenu
				}
			}
			break;
		case kBlue:
			if (strcmp(Get(Current() )->Text(), tr("similarArtist") ) == 0 || strcmp(Get(Current())->Text(), tr("Tag") )) {
				AddSubMenu(new cUserInterface_Search_Favourites(&m_searchword[0]) );
			}
			break;
		default:
			state = osContinue;
			break;
		}
	}
	return state;
}


