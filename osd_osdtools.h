/*
 * osd_osdtools.h: OSD helper functions
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: osd_osdtools.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __OSDTOOLS_H
#define __OSDTOOLS_H

#include <string>
#include <vdr/osd.h>
#include <vdr/interface.h>

class cOsdTools
{
public:
	cOsdTools(cOsd * osd_OsdTools);
	void DrawTextfield(const int x, const int y, const std::string Caption, const tColor Color);
	void DrawButton(const int x, const int y, const std::string Caption, const tColor Color,
			const bool pressed = false);
	void SetHelpButtons(void);
	void BevelButton(const int x, const int y, const std::string Caption, const tColor Color);
private:
	cOsd * osd;
private:
	// Osd_Tools:
	int m_Osd_Width;
	int m_Osd_Height;
	int m_Osd_ColourDepth;
	int m_Osd_AreaDepth;
	int m_Osd_Centre_x;
	int m_Osd_Centre_y;
	int m_Osd_BitmapSize_x;
	int m_Osd_BitmapSize_y;
	int m_Osd_ImgAlpha;
	int m_Osd_BtnWidth;
	int m_Osd_FontHeight;
};

#endif //__OSDTOOLS_H
