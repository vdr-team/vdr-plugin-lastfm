/**
 * setup.c: Setup entry point for creating/selecting profiles
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: setup.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include "utils.h"
#include "setup.h"
#include "setup_favourites.h"
#include "settings_xml.h"
#include "setup_parameters.h"

cSetupLastfm::cSetupLastfm(void)
{
	BuildItems();
}


void cSetupLastfm::BuildItems(void)
{
	strncpy(newProfile, "", 511);

	Clear();
	SetHelp(0, 0, trVDR("Button$Delete") );


	// Profileselection (or creation of a new profile):
	std::list<std::string> profilenames = cXmlconfig::GetProfilenames();

	for (std::list<std::string>::iterator it = profilenames.begin(); it
	     != profilenames.end(); it++) {
		std::string it_string = *it;

		if (!it_string.empty() )
			Add(new cOsdItem(it_string.c_str() ) );
	}


	// Add an entry, that offers profile creation:
	m_newentryitem = new cMenuEditStrItem(tr("New profile"),
					      newProfile, sizeof( newProfile ), tr(cUtils::GetFileNameChars_lastfm()) );
	Add(m_newentryitem);

}


void cSetupLastfm::Store(void)
{

}


eOSState cSetupLastfm::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);


	if (!HasSubMenu() && state == osUnknown) {
		/** display SetHelp buttons according to the lists */
		if (m_newentryitem == Get(Current() ) ) {   // last entry
			SetHelp(0);
		} else {
			SetHelp(0, 0, trVDR("Button$Remove") );
		}

		switch (Key) {
		case kOk:
			if (m_newentryitem == Get(Current() ) ) {               // last entry
				if (strcmp(newProfile, "") ) {                  // string shall not be empty
					Ins(new cOsdItem(newProfile), false, m_newentryitem);

					return AddSubMenu(new cSetupParameters(newProfile) );
				}

				state = osContinue;
				break;
			} else {
				return AddSubMenu(new cSetupParameters(Get(Current() )->Text() ) );
			}

		case kYellow:
			if (m_newentryitem == Get(Current() ) ) {
				if (strcmp(newProfile, "") ) {
					if (!cXmlconfig::DeleteProfile(newProfile) ) {
						Del(Current());
						Display();
					}
				}
			} else if (!cXmlconfig::DeleteProfile(Get(Current() )->Text() ) ) {
				Del(Current());
				Display();
			}
			break;

		default:
			state = osContinue;
			break;
		}
	}
	return state;
}


