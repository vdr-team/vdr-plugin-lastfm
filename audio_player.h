/*
 * audio_player.h: The audio player and control interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-11 02:35:09  mf $
 */

#ifndef __AUDIO_PLAYER_H
#define __AUDIO_PLAYER_H

#include <stdint.h>
#include <vdr/player.h>
#include "audio_decoder_mp3_http.h"
#include "profile.h"

class cAudioPlayer : public cPlayer, public cThread, public cProfile
{
private:
	cHttpMp3Decoder * httpmp3decoder;
private:
	enum ePlayMode {pmPlay=0, pmPaused, pmStopped, pmBuffered, pmNone};
	ePlayMode m_playMode;
	//! \brief a buffer for decoded sound
	cRingBufferFrame *m_ringbuffer;
	cCondVar mo_playerCond;
	cMutex mo_playerMutex;
	void Clear(void);
	void SetPlayMode(ePlayMode mode);
	void Quit(void);
protected:
	virtual void Action(void);
public:
	cAudioPlayer(void);
	virtual ~cAudioPlayer(void);
	void Stop(void);
	void Next(void);
	virtual bool GetIndex(int & Current, int & Total, bool SnapToIFrame = false);
private:
	std::string m_nextsong;
};

#endif // __AUDIO_PLAYER_H
