/*
 * service.h: Service interface for other plugins that want to scrobble music
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2010-11-20 15:17:42  mf $
 */

#ifndef __SERVICE_H
#define __SERVICE_H

typedef uint8_t (*Submission_LastfmHandle)(const std::string track, const std::string timestamp, const std::string artist,
						const std::string sk);

typedef uint8_t (*PlayingNow_LastfmHandle)(const std::string track, const std::string artist,
						const std::string sk);

typedef uint8_t (*Handshake_LastfmHandle)(const std::string username,
							   const std::string passphrase_md5,
							   std::string &websvcs_session,
							   std::string &authtoken);

struct SubmissionHandle_v1_1 {
	Submission_LastfmHandle Submission;
	PlayingNow_LastfmHandle PlayingNow;
	Handshake_LastfmHandle Handshake;
};

static const char
* const LASTFM_SUBMISSION_HANDLE_v1_1 = "lastfm_submission_v1_1";

#endif // __SERVICE_H
