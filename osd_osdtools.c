/*
 * osd_osdtools.c: OSD helper functions
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: osd_osdtools.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <vdr/plugin.h>
#include "osd_osdtools.h"

cOsdTools::cOsdTools(cOsd * osd_OsdTools)
{
	osd = osd_OsdTools;

	// Osd_Tools:
	m_Osd_Width = 480;
	m_Osd_Height = 360;
	m_Osd_AreaDepth = 4;
	m_Osd_ColourDepth = 16;
	m_Osd_Centre_x = m_Osd_Width / 2;
	m_Osd_Centre_y = m_Osd_Height / 2;
	m_Osd_BitmapSize_x = 180;
	m_Osd_BitmapSize_y = 180;
	m_Osd_ImgAlpha = 255;
	m_Osd_BtnWidth = m_Osd_Width / 4;
	m_Osd_FontHeight = 20;

}


void cOsdTools::DrawTextfield(const int x, const int y,
			      const std::string Caption, const tColor Color)
{
	int iBtnHeight = 28;
	int iBtnWidth = m_Osd_BtnWidth * 2; //130;

	// Drawing this rectangle, deletes previous drawn bits and bytes:
	osd->DrawRectangle(x + 1, y + 1, x + iBtnWidth - 1, y + iBtnHeight - 1,
			   clrTransparent);
	osd->DrawText(x, y, Caption.c_str(), clrWhite, clrTransparent,
		      cFont::GetFont(fontSml) );
}


void cOsdTools::DrawButton(const int x, const int y,
			   const std::string Caption, const tColor Color, const bool pressed)
{
	int iBtnHeight = 28;
	int iBtnWidth = m_Osd_BtnWidth; //m_Osd_Width / 4;

	if (!pressed) {
		osd->DrawText(x, y, Caption.c_str(), clrWhite, Color,
			      cFont::GetFont(fontSml), iBtnWidth, iBtnHeight - 6, taCenter);
	} else {
		osd->DrawRectangle(x + 1 - 5, y + 1 - 5, x + iBtnWidth - 1, y + iBtnHeight - 1, Color);
		osd->DrawText(x, y, Caption.c_str(), clrWhite, Color,
			      cFont::GetFont(fontSml), iBtnWidth, iBtnHeight - 6, taCenter);
	}

}


void cOsdTools::SetHelpButtons(void)
{
	int BtnWidth = m_Osd_BtnWidth;
	int Osd_Height = m_Osd_Height;


	//Die Mitte der Buttens ist der Offset
	DrawButton(0, Osd_Height - 20, tr("Love"), clrRed);
	DrawButton(0 + BtnWidth, Osd_Height - 20, tr("Skip"), clrGreen);
	DrawButton(0 + BtnWidth + BtnWidth, Osd_Height - 20, tr("Ban"),
		   clrYellow);
	DrawButton(0 + BtnWidth + BtnWidth + BtnWidth, Osd_Height - 20,
		   tr("Exit"), clrBlue);
}


void cOsdTools::BevelButton(const int x, const int y,
			    const std::string Caption, const tColor Color)
{
	DrawButton(x, y, Caption, Color, true);
	osd->Flush();
	cCondWait::SleepMs(500);
	osd->DrawRectangle(0, m_Osd_Height - m_Osd_FontHeight - 5, m_Osd_Width, m_Osd_Height, clrTransparent);
	SetHelpButtons();
	osd->Flush();
}


