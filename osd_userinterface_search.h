/**
 * osd_userinterface_search.h: Search for artists and tags
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-14 13:41:08  mf $
 */

#ifndef __USERINTERFACE_SEARCH_H
#define __USERINTERFACE_SEARCH_H

#include <vdr/menu.h>
#include "web_serviceconnector.h"
#include "config.h"

class cUserInterface_Search : public cOsdMenu
{
private:
	void BuildItems(void);
	char m_searchword[ SEARCHWORD_MAXSIZE ];
public:
	cUserInterface_Search(void);
	virtual eOSState ProcessKey(eKeys Key);
};

#endif //__USERINTERFACE_SEARCH_H

