/**
 * setup_favourites.h: Setup submenu for creating/deleting favourite entries
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: setup_favourites.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __SETUP_FAVOURITES_H
#define __SETUP_FAVOURITES_H

#include <vdr/menu.h>
#include <string>
#include <list>

class cSetupFavourites : public cOsdMenu
{
public:
	cSetupFavourites(std::list<std::string> &FavouriteList);
private:
	void BuildItems(void);
	char newfavourite[ 512 ];
	std::list<std::string> & m_FavouriteList;
protected:
	virtual eOSState ProcessKey(eKeys Key);
};

#endif //__SETUP_FAVOURITES_H
