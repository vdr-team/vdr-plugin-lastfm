/**
 * osd_userinterface_metadata.h: Presents submenus with metadata items
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-13 22:05:09  mf $
 */

#ifndef __USERINTERFACE_METADATA_H
#define __USERINTERFACE_METADATA_H

#include <vdr/menu.h>

class cUserInterface_Metadata : public cOsdMenu
{
private:
	void BuildItems(void);
public:
	cUserInterface_Metadata(void);
	virtual eOSState ProcessKey(eKeys Key);
};

#endif //__USERINTERFACE_METADATA_H

