/*
 * audio_filter.h:  The audio filter interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-01 11:34:50  mf $
 */
/*!
 * \file vdr_sound.c
 * \brief Sound manipulation classes for a VDR media plugin (muggle)
 *
 * \version $Revision: 1.2 $
 * \date    $Date$
 * \author  Ralf Klueber, Lars von Wedel, Andreas Kellner
 * \author  Responsible author: $Author$
 *
 * $Id$
 *
 * Adapted from
 * MP3/MPlayer plugin to VDR (C++)
 * (C) 2001,2002 Stefan Huelswitt <huels@iname.com>
 */

#ifndef __AUDIO_FILTER_H
#define __AUDIO_FILTER_H

#include <mad.h>

// --- mgScale ----------------------------------------------------------------

// The dither code has been adapted from the madplay project
// (audio.c) found in the libmad distribution

enum eAudioMode
{ amRound, amDither };

class mgScale
{
	private:
		enum
		{ MIN = -MAD_F_ONE, MAX = MAD_F_ONE - 1 };
#ifdef DEBUG
		// audio stats
		unsigned long clipped_samples;
		mad_fixed_t peak_clipping;
		mad_fixed_t peak_sample;
#endif
		// dither
		struct dither
		{
			mad_fixed_t error[3];
			mad_fixed_t random;
		} leftD, rightD;
		//
		inline mad_fixed_t Clip (mad_fixed_t sample, bool stats = true);
		inline signed long LinearRound (mad_fixed_t sample);
		inline unsigned long Prng (unsigned long state);
		inline signed long LinearDither (mad_fixed_t sample, struct dither *dither);
	public:
		mgScale();
		void Stats (void);
		unsigned int ScaleBlock (unsigned char *data, unsigned int size,
			unsigned int &nsamples, const mad_fixed_t * &left,
			const mad_fixed_t * &right, eAudioMode mode);
};

// --- mgResample ------------------------------------------------------------

// The resample code has been adapted from the madplay project
// (resample.c) found in the libmad distribution


class mgResample
{
	private:
		mad_fixed_t ratio;
		mad_fixed_t step;
		mad_fixed_t last;
		mad_fixed_t resampled[MAX_NSAMPLES];
	public:
		bool SetInputRate (unsigned int oldrate, unsigned int newrate);
		unsigned int ResampleBlock (unsigned int nsamples, const mad_fixed_t * old);
		const mad_fixed_t *Resampled (void) {
			return resampled;
		}
};

#endif /** __AUDIO_FILTER_H */

