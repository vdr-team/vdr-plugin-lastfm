/**
 * osd_userinterface_data_getlovedtracks.h: Presents loved tracks. 
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-15 13:08:13  mf $
 */

#ifndef __USERINTERFACE_DATA_GETLOVEDTRACKS_H
#define __USERINTERFACE_DATA_GETLOVEDTRACKS_H

#include <vdr/menu.h>

class cUserInterface_Data_getLovedTracks : public cOsdMenu
{
private:
	void BuildItems();
public:
	cUserInterface_Data_getLovedTracks(void);
};

#endif //__USERINTERFACE_DATA_GETLOVEDTRACKS_H

