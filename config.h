// General
#define PLUGIN_NAME "VDR-LASTFM"
/*
 * Before releasing, unset DEBUGFLAGS in Makefile!
 */
#ifndef PLUGIN_VERSION
#define PLUGIN_VERSION "0.2.6"
#endif

// web_serviceconnector
#define AUDIOSCROBBLER_CLIENTID "vdr"
#define AUDIOSCROBBLER_CLIENTVERSION "0.1"
#define HTTPGET_MAXSIZE 120000 /** Bytes (XML data can be big) */
#define AUDIOSCROBBLER_DOMAIN "ws.audioscrobbler.com"
#define AUDIOSCROBBLER_BASEPATH "/radio"
#define AUDIOSCROBBLER_BASEPATH_WEBSVCS "/2.0/"
#define AUDIOSCROBBLER_POST_DOMAIN "post.audioscrobbler.com"
#define AUDIOSCROBBLER_PORT 80
#define AUDIOSCROBBLER_PROTOCOLVER "1.2"
#define APIKEY "e0e0b4f1087241330ec23cb921da1f59"
#define APISECRET "a185973f1fd2ebe3cd631eb1afa86e62"

// audio_player
#define PLAYER_BUFSIZE 2400000  /** cFrameRingBuffer */
#define MAX_FRAMESIZE   2048	 // max. frame size allowed for DVB driver
#define POLLTIMEOUT 100
#define SOCKETS_SELECT_US 3*1000        // Refresh cycle in Thread; datarate@17.05 kB/s
#define NET_READBUFFSIZE 2048
#define HDR_SIZE        9
#define LPCM_SIZE       7
#define LEN_CORR        3

// audio_decoder
#define READBUFFSIZE 2048                       // buffersize in audio_mp3decoder.h for readbuffer
#define NET_BUFFER_SIZE (1300000)

//
#define MAXIMUM_THREADKILLTIME 3 // cancel timeout in seconds

// audio_playlist
#define GETPLAYLIST_DELAY 4

// Setup
#define SETUP_CONFIGFILE_XML "lastfm_config.xml"

// Favourites
#define SEARCHWORD_MAXSIZE 511

// mgResample
#define OUT_BITS 16
#define OUT_FACT (OUT_BITS/8*2)	 // output factor is 16 bit & 2 channels -> 4 bytes
// mgResample
#define MAX_NSAMPLES (1152*7)	 // max. buffer for resampled frame
#define OUT_BITS 16				 // output 16 bit samples to DVB driver
#define OUT_FACT (OUT_BITS/8*2)	 // output factor is 16 bit & 2 channels -> 4 bytes

// ParameterMap
#define PARAMETERMAP_USERNAME "Username"
#define PARAMETERMAP_PASSPHRASE_MD5 "Passphrase_md5"
#define PARAMETERMAP_CURRENTPROFILENAME "CurrentProfilename"
#define PARAMETERMAP_PROFILENAMETOLOAD "ProfilenameToLoad"
#define PARAMETERMAP_RECEIVERMODE "Receivermode"
#define PARAMETERMAP_RUNNING "Running"
#define PARAMETERMAP_SCROBBLEMODE "Scrobblemode"
#define PARAMETERMAP_ALBUMCOVERMODE "Albumcovermode"
#define PARAMETERMAP_OSD_MODE "Osd_Mode"
#define PARAMETERMAP_OSD_AUTOSTARTMODE "Osd_Autostartmode"
#define PARAMETERMAP_OSD_REFRESHINTERVAL "Osd_Refreshinterval"
#define PARAMETERMAP_PLAYER_BUFFERTIME "Player_Buffertime"

