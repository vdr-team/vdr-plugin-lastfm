/**
 * osd_userinterface_data_artistMetadata.h: Presents metadata
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-15 13:08:13  mf $
 */

#ifndef __USERINTERFACE_DATA_ARTISTMETADATA_H
#define __USERINTERFACE_DATA_ARTISTMETADATA_H

#include <vdr/menu.h>

class cUserInterface_Data_artistMetadata : public cMenuText
{
private:
	void BuildItems();
public:
	cUserInterface_Data_artistMetadata(void);
};

#endif //__USERINTERFACE_DATA_ARTISTMETADATA_H
