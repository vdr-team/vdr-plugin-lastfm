/**
 * web_networkcontroller.c: Get,Post,PostXML-Networkmethods
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <string.h>
#include "web_networkcontroller.h"
#include "web_networkcontroller_httpget.h"
#include "web_networkcontroller_httppost.h"
#include "web_networkcontroller_httppostxml.h"
#include "config.h" // HTTPGET_MAXSIZE

/**
 *
 * The class below, describes a HTTPget method:
 */
bool cNetworkController::HttpGetRequest(std::string & body,
					const std::string url_in)
{
	SocketHandler h;
	bool complete = false;
	std::string document;

	My_Web_HttpGetSocket s(h, url_in);
	unsigned char * p = new unsigned char[ HTTPGET_MAXSIZE ]; // max doc size

	memset(p, 0, HTTPGET_MAXSIZE);
	s.SetDataPtr(p, HTTPGET_MAXSIZE - 1); // preserve zero at end of document
	h.Add(&s);

	h.Select(0, 1);

	while (h.GetCount() ) {
		h.Select(0, 1);
	}

	complete = s.Complete();

	if (complete && strcmp( (char *)s.GetDataPtr(), "") != 0) {
		document = static_cast<std::string>( (char *)s.GetDataPtr());
	} else
		return 1;

	delete[] s.GetDataPtr();

	body = document;

	return complete;
}


/**
 *
 * The class below, describes a HTTPpost method:
 */
bool cNetworkController::HttpPostRequest(std::string & body,
					 const std::string url, std::map<std::string, std::string> postdataMap)
{
	std::string document;
	SocketHandler h;

	bool complete = false;
	unsigned char * p_post = new unsigned char[HTTPGET_MAXSIZE]; // max doc size

	My_Web_HttpPostSocket s_post(h, url);

	for (std::map<std::string, std::string>::iterator
	     theIterator = postdataMap.begin(); theIterator
	     != postdataMap.end(); theIterator++) {
		s_post.AddField( (*theIterator).first, (*theIterator).second);
	}

	/**
	 * TODO: needs further investigation: a second search immediately triggered after a tag search
	 * leads to a crash when not commented out following line.
	   /////	s_post.SetCloseOnComplete( true );
	 */
	s_post.Open();

	memset(p_post, 0, HTTPGET_MAXSIZE);
	s_post.SetDataPtr(p_post, HTTPGET_MAXSIZE - 1);   // preserve zero at end of document

	h.Add(&s_post);
	h.Select(0, 1);

	while (h.GetCount() ) {
		h.Select(0, 1);
	}

	complete = s_post.Complete();

	if (complete) {
		document = static_cast<std::string>( (char *)s_post.GetDataPtr());
	}

	delete[] s_post.GetDataPtr();

	body = document;

	return complete;
}


/**
 *
 * The class below, describes a HTTPpost XML method:
 */
bool cNetworkController::HttpPostXmlRequest(std::string & body,
					    const std::string url, const std::string xmldata)
{
	std::string document;
	SocketHandler h;

	bool complete = false;
	unsigned char * p_post = new unsigned char[HTTPGET_MAXSIZE]; // max doc size

	My_Web_HttpPostXmlSocket s_post(h, url, xmldata);

	s_post.Open();

	memset(p_post, 0, HTTPGET_MAXSIZE);
	s_post.SetDataPtr(p_post, HTTPGET_MAXSIZE - 1);   // preserve zero at end of document

	h.Add(&s_post);
	h.Select(0, 1);

	while (h.GetCount() ) {
		h.Select(0, 1);
	}

	complete = s_post.Complete();

	if (complete) {
		document = static_cast<std::string>( (char *)s_post.GetDataPtr());
	}

	delete[] s_post.GetDataPtr();

	body = document;

	return complete;
}


