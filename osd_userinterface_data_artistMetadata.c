/**
 * osd_userinterface_data_artistMetadata.c: Presents metadata
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-15 19:53:27  mf $
 */

#include <string>
#include <map> // MetadataMap
#include "osd_userinterface_data_artistMetadata.h"
#include "web_serviceconnector_webservices.h"
#include "config.h" // API_KEY
#include "logdefs.h"
#include <vdr/tools.h>
#include "utils.h" // html2text

extern std::map<std::string, std::string> MetadataMap;

cUserInterface_Data_artistMetadata::cUserInterface_Data_artistMetadata(void) :
	cMenuText("", "")
{
	BuildItems();  // OSD-Elemente aufbauen
}


void cUserInterface_Data_artistMetadata::BuildItems(void)
{
	std::string artist(MetadataMap["track_creator"]);
	std::string lang(tr("en"));
	std::string mbid;
	std::string api_key(APIKEY);
	std::string content;

	SetTitle(artist.c_str());
	if (!cWebServiceConnectorWebservices::artist.getInfo(artist, mbid, lang, api_key, content)) {
		cUtils::html2text(content, content);
		SetText(content.c_str() );
	}
#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cUserInterface_Data_artistMetadata::BuildItems:content: %s\r\n", content.c_str());
#endif
	Display();
}


