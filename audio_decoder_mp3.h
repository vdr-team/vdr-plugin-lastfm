/*
 * audio_decoder_mp3.h: The mp3 decoder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-11 12:24:28  mf $
 */

#ifndef __AUDIO_DECODER_MP3_H
#define __AUDIO_DECODER_MP3_H

#include <inttypes.h>
#include <string.h>
#include <stdint.h>
#include <mad.h>
#include "config.h"

/**
 *
 * cMP3Decoder opens an iStream (filestream or netstream) and
 * puts the data pieces on the mad_stream_decoder.
 * The mad_decoder will fill the output with PCM data (held in madSynth).
 *
 * ISTREAM ---> MAD_DECODER ---> PCMBuffer
 */
class cMp3Decoder
{
public:
	cMp3Decoder(void);
	virtual ~cMp3Decoder();
	mad_pcm *GetData(void);
	/**
	 * GetIndex
	 * @param: reference to the current position of the current track
	 * @return: errorcode
	 */
	int GetIndex(int & Current, int & Total, bool SnapToIFrame = false);
	uint8_t Status(void);
private:
	int FillReadBuffer();
	/**
	 * DecodeNextChunk loads data from mp3-source and
	 * decodes that data.
	 * After that, it calls MadResample and in the latter function,
	 * the pointers according to the decoded data will be set.
	 */
	void DecodeNextChunk();
	virtual uint32_t GetDataFromSource(uint8_t * data, const uint32_t count) = 0;
private:
	static const int readBufferSize = READBUFFSIZE;
	unsigned char readBuffer[readBufferSize];
	struct mad_stream madStream;
	struct mad_frame madFrame;
	struct mad_synth madSynth;
	mad_timer_t timeIndex;
	uint8_t m_sourcedataavailable;
};

#endif // __AUDIO_DECODER_H
