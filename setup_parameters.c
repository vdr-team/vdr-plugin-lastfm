/**
 * setup_parameters.c: Setup submenu for changing user parameters
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-17 10:18:00  mf $
 */

#include "setup_parameters.h"
#include "utils.h"
#include "settings_xml.h"
#include "setup_favourites.h"
#include "config.h"

extern std::map<std::string, std::string> ParameterMap;
extern std::list<std::string> FavouriteList;

cSetupParameters::cSetupParameters(const std::string profilename)
	: m_profilename(profilename)
{
	// Load certain profile:
	std::map<std::string, std::string> ParameterMap;

	cXmlconfig::Load(profilename, ParameterMap, m_FavouriteList);

	if (!ParameterMap[PARAMETERMAP_USERNAME].compare("") )   // no username defined
		ParameterMap[PARAMETERMAP_USERNAME] = profilename;

	strncpy(newUsername, ParameterMap[PARAMETERMAP_USERNAME].c_str(), sizeof(newUsername) - 1);
	newReceivermode = atoi(ParameterMap[PARAMETERMAP_RECEIVERMODE].c_str() );
	newAlbumcovermode = atoi(ParameterMap[PARAMETERMAP_ALBUMCOVERMODE].c_str() );
	newOsd_Mode = atoi(ParameterMap[PARAMETERMAP_OSD_MODE].c_str() );
	newOsd_Autostartmode = atoi(ParameterMap[PARAMETERMAP_OSD_AUTOSTARTMODE].c_str() );
	newOsd_Refreshinterval = atoi(ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL].c_str() );
	newScrobblemode = atoi(ParameterMap[PARAMETERMAP_SCROBBLEMODE].c_str() );
	newPlayer_Buffertime = atoi(ParameterMap[PARAMETERMAP_PLAYER_BUFFERTIME].c_str() );


	// StraItem-Arrays:
	m_array_Receivermode[0] = (char *)trVDR("Button$Off");
	m_array_Receivermode[1] = (char *)tr("SoftdeviceReceiver");
	m_array_Osd_Mode[0] = (char *)tr("graphical");
	m_array_Osd_Mode[1] = (char *)tr("textual");

	SetHelp(0, 0, 0, 0);
	BuildOsdItems();
}


void cSetupParameters::BuildOsdItems(void)
{
	Clear();
	Add(new cOsdItem(hk(tr("Favourites...") ) ) );

	///newpassword leeren, damit es nicht im Menue als Klartext angezeigt wird.
	memset(&newPassphrase, 0, sizeof(newPassphrase) );

	Add(new cMenuEditStrItem(tr("Username"),
				 newUsername, sizeof( newUsername ), tr(cUtils::GetFileNameChars_lastfm()) ) );
	Add(new cMenuEditStrItem(tr("Passphrase"),
				 newPassphrase, sizeof( newPassphrase ), tr(cUtils::GetFileNameChars_lastfm()) ) );
#ifdef LASTFM_HAVE_MAGICK
	Add(new cMenuEditBoolItem(tr("Show albumcover?"), &newAlbumcovermode) );
#endif  //LASTFM_HAVE_MAGICK
	Add(new cMenuEditStraItem(tr("Osdmode"), &newOsd_Mode, 2, m_array_Osd_Mode) );
	Add(new cMenuEditIntItem(tr("Osd refresh interval"), &newOsd_Refreshinterval, 0, 60) );
	Add(new cMenuEditBoolItem(tr("Scrobblemode"), &newScrobblemode) );
	Add(new cMenuEditBoolItem(tr("Show Osd at startup?"), &newOsd_Autostartmode) );
	Add(new cMenuEditIntItem(tr("Player buffertime"), &newPlayer_Buffertime) );
}


void cSetupParameters::Store(void)
{
	// check, whether we edit an active user using this plugin, and fill the
	// parameters according to his profile:
	if (ParameterMap["CurrentProfileName"].compare(m_profilename) == 0) {
		std::map<std::string, std::string> ParameterMap;

		ParameterMap[PARAMETERMAP_USERNAME] = newUsername;

		// Passwort nicht mitspeichern, wenn es leer gelassen wurde.
		if (strcmp(newPassphrase, "") != 0) {
			ParameterMap[PARAMETERMAP_PASSPHRASE_MD5] = cUtils::MD5(newPassphrase);
		}

		// reset password
		strcpy(newPassphrase, "");
#ifdef LASTFM_HAVE_MAGICK
		ParameterMap[PARAMETERMAP_ALBUMCOVERMODE] = itoa(newAlbumcovermode);
#endif          //LASTFM_HAVE_MAGICK
		ParameterMap[PARAMETERMAP_OSD_MODE] = itoa(newOsd_Mode);
		ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL] = itoa(newOsd_Refreshinterval);
		ParameterMap[PARAMETERMAP_OSD_AUTOSTARTMODE] = itoa(newOsd_Autostartmode);
		ParameterMap[PARAMETERMAP_SCROBBLEMODE] = itoa(newScrobblemode);
		ParameterMap[PARAMETERMAP_PLAYER_BUFFERTIME] = itoa(newPlayer_Buffertime);

		// Store the data to the XML configfile:
		cXmlconfig::Save(m_profilename, ParameterMap, m_FavouriteList);
	} else {
/////		if (ParameterMap["Username"].compare(newUsername))
			ParameterMap[PARAMETERMAP_USERNAME] = newUsername;


		// Passwort nicht mitspeichern, wenn es leer gelassen wurde.
		if (strcmp(newPassphrase, "") != 0) {
			ParameterMap[PARAMETERMAP_PASSPHRASE_MD5] = cUtils::MD5(newPassphrase);
		}

		// reset password
		strcpy(newPassphrase, "");

/**
 * Set parameters live into ParameterMap, used by current profile:
 */
#ifdef LASTFM_HAVE_MAGICK
		if (ParameterMap[PARAMETERMAP_ALBUMCOVERMODE].compare(itoa(newAlbumcovermode) ) )
			ParameterMap[PARAMETERMAP_ALBUMCOVERMODE] = itoa(newAlbumcovermode);
#endif          //LASTFM_HAVE_MAGICK
		if (ParameterMap[PARAMETERMAP_OSD_MODE].compare(itoa(newOsd_Mode) ) )
			ParameterMap[PARAMETERMAP_OSD_MODE] = itoa(newOsd_Mode);

		if (ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL].compare(itoa(newOsd_Refreshinterval) ) )
			ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL] = itoa(newOsd_Refreshinterval);

		if (ParameterMap[PARAMETERMAP_OSD_AUTOSTARTMODE].compare(itoa(newOsd_Autostartmode) ) )
			ParameterMap[PARAMETERMAP_OSD_AUTOSTARTMODE] = itoa(newOsd_Autostartmode);

		if (ParameterMap[PARAMETERMAP_SCROBBLEMODE].compare(itoa(newScrobblemode) ) )
			ParameterMap[PARAMETERMAP_SCROBBLEMODE] = itoa(newScrobblemode);

		if (ParameterMap[PARAMETERMAP_PLAYER_BUFFERTIME].compare(itoa(newPlayer_Buffertime) ) )
			ParameterMap[PARAMETERMAP_PLAYER_BUFFERTIME] = itoa(newPlayer_Buffertime);

		FavouriteList = m_FavouriteList;

		// Store the data to the XML configfile:
		cXmlconfig::Save(m_profilename, ParameterMap, m_FavouriteList);
	}
}


eOSState cSetupParameters::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);

	switch (state) {
	/** when user presses kBack, setup shall store even then,
	 * because of the favourites */
	case osBack:
		Store();
		break;

	case osUnknown:
		if (Key == kOk) {
			if (!strcmp(Get(Current())->Text(), tr("Favourites...") ) ) {
				return AddSubMenu(new cSetupFavourites(m_FavouriteList) );
			} else {
				Store();
				state = osBack;
			}
		}
		break;

	default:
		break;
	}

	return state;
}


