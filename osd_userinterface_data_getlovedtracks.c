/**
 * osd_userinterface_data_recenttracks.c: Presents metadata
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-16 12:52:08  mf $
 */

#include "osd_userinterface_data_getlovedtracks.h"
#include <string>
#include <map> // MetadataMap
#include "osd_userinterface_data_artistMetadata.h"
#include "web_serviceconnector_webservices.h"
#include "config.h" // API_KEY
#include "logdefs.h"
#include <vdr/tools.h>
#include <inttypes.h>
#include "ext/xmlParser/xmlParser.h"
#include <sstream>

extern std::map<std::string, std::string> ParameterMap; // username

cUserInterface_Data_getLovedTracks::cUserInterface_Data_getLovedTracks(void)
:cOsdMenu( tr("recentlovedtracks"), 20){
	BuildItems();  // OSD-Elemente aufbauen
}


void cUserInterface_Data_getLovedTracks::BuildItems(void)
{
	std::string user(ParameterMap[PARAMETERMAP_USERNAME]);
	std::string api_key(APIKEY);
	std::string content;
	std::stringstream sstream_tmp;

	if (!cWebServiceConnectorWebservices::user.getLovedTracks(content, user, api_key)) {
		/** parsing received xml data: */
	XMLNode xMainNode, xNode;
	XMLResults xe;
	xMainNode = XMLNode::parseString(content.c_str(), "lfm", &xe);

  if (xe.error)
     return;

	xNode = xMainNode.getChildNode("lovedtracks");

  int n = xNode.nChildNode("track");

	for (int i=0; i<n; i++)
	{
		sstream_tmp.str("");
		sstream_tmp.clear();

		sstream_tmp << xNode.getChildNode("track",i).getChildNode("artist").getChildNode("name").getText();
		sstream_tmp << " - ";
		sstream_tmp << xNode.getChildNode("track",i).getChildNode("name").getText();

	      Add(new cOsdItem(hk(sstream_tmp.str().c_str())));
	}

	}

	Display();
}

