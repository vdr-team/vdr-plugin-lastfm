/*
 * audio_control.c: The control interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: audio_control.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __AUDIO_CONTROL_H
#define __AUDIO_CONTROL_H

#include <vdr/player.h>
#include "audio_player.h"
#include "osd_userinterface_submenu.h"
#include "osd_osdtools.h"
#include <vector> // for menu status entries
#include <string>
#include <sstream>

class cAudioControl : public cControl, public cThread
{
private:
	cAudioPlayer * m_player;
	cOsd * m_osd;
	cOsdTools * m_OsdTools;
	cSkinDisplayMenu * m_displayMenu;
	int m_visible;
	cUserInterface_Submenu * m_UserInterface_Submenu;
#ifdef LASTFM_HAVE_GRAPHTFT
	void SendGtftHelpButtons(const char * red, const char * green, const char * yellow, const char * blue);
#endif
	std::vector<std::string> m_menuitems;
	unsigned int m_currentmenuitem;
protected:
	void BuildMenu(void);
	void SetHelp(void);
	void SetTitle(void);
	void SetOsd(void);
	void AddItem(const std::string itemtext);
	virtual void Action(void);
public:
	cAudioControl(void);
	~cAudioControl();
	virtual void Hide(void);
	virtual eOSState ProcessKey(eKeys Key);
	virtual void Show(void);
private:
	// Osd_Tools:
	int m_Osd_Width;
	int m_Osd_Height;
	int m_Osd_ColourDepth;
	int m_Osd_AreaDepth;
	int m_Osd_Centre_x;
	int m_Osd_Centre_y;
	int m_Osd_BitmapSize_x;
	int m_Osd_BitmapSize_y;
	int m_Osd_ImgAlpha;
	int m_Osd_BtnWidth;
	int m_Osd_FontHeight;
};

#endif // __AUDIO_CONTROL_H
