/**
 * osd_userinterface_search_favourites.h: Search submenu for selecting favourites
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-13 22:06:30  mf $
 */

#ifndef __USERINTERFACE_SEARCH_FAVOURITES_H
#define __USERINTERFACE_SEARCH_FAVOURITES_H

#include <vdr/menu.h>

class cUserInterface_Search_Favourites : public cOsdMenu
{
private:
	char * m_searchword_ptr;
	void BuildItems(void);
public:
	cUserInterface_Search_Favourites(char * searchword_ptr);
	virtual eOSState ProcessKey(eKeys Key);
};

#endif //__USERINTERFACE_SEARCH_FAVOURITES_H
