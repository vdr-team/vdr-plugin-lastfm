/**
 * web_networkcontroller_httppostxml.h: HTTPPostXML
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httppostxml.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef WEB_NETWORKCONTROLLER_HTTPPOSTXML_H_
#define WEB_NETWORKCONTROLLER_HTTPPOSTXML_H_

#include <string.h>
#include <stdlib.h>
#include <Sockets/HttpPostSocket.h>

class My_Web_HttpPostXmlSocket : public HttpPostSocket
{
public:
	My_Web_HttpPostXmlSocket(ISocketHandler &, const std::string & url_in, const std::string xmldata);
private:
	std::string _xmldata;
	virtual std::string MyUseragent();
	virtual void OnConnect(void);
};

#endif // WEB_NETWORKCONTROLLER_HTTPPOSTXML_H_
