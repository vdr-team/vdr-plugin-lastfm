/**
 * playlist.h: Playlist
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-08-22 19:51:56  mf $
 */

#ifndef __AUDIO_PLAYLIST_H
#define __AUDIO_PLAYLIST_H

#include <queue> // FIFO list
#include <string>
#include <map>

class cPlaylist
{
public:
	virtual ~cPlaylist();
	std::queue<std::map<long, std::map<std::string, std::string> > > m_trackList;
	virtual std::string GetNextsongurl(void);
	virtual int PlsClear(void);
};

#endif // __AUDIO_PLAYLIST_H
