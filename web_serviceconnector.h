/*
 * web_serviceconnector.h: Desc
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_serviceconnector.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __WEB_SERVICECONNECTOR_H
#define __WEB_SERVICECONNECTOR_H

#include <string>
#include <queue>
#include <map>

class cWebServiceConnector
{
public:
	/**
	 * Handshake
	 * in order to get a sessionID, that can be used to retrieve an xspf.php-Playlist.
	 *
	 * @param: std::string username
	 * @param: std::string password_md5
	 * @return: errorcode
	 */
	static int Handshake(const std::string username, const std::string password_md5,
			     std::string & session, int & subscriber);
	/**
	 * GetPlaylist
	 *
	 * @param: session
	 * @return: 0==noerror; 2==network error; 3==error while parsing data
	 */
	static int GetPlaylist(const std::string session,
			       std::queue<std::map<long, std::map<std::string, std::string> > > & trackList);

	/**
	 * Changestation
	 */
	static int Changestation(const std::string session, const std::string lastfm_url);
};

#endif // __WEB_SERVICECONNECTOR_H

