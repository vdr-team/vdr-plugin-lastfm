/**
 * osd_userinterface_submenu.c: Submenu for several items
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-16 12:51:39  mf $
 */

#include <vdr/remote.h>
#include "osd_userinterface_submenu.h"
#include <vdr/tools.h>
#include "logdefs.h"
#include "osd_userinterface_search.h"
#include "osd_userinterface_metadata.h"
#include <sstream>

extern std::map<std::string, std::string> ParameterMap; // username
extern std::map<std::string, std::string> MetadataMap;  // session


cUserInterface_Submenu::cUserInterface_Submenu(void)
	: cOsdMenu(tr("Submenu"), 20)
{
	// OSD-Elemente aufbauen
	BuildItems();
}


cUserInterface_Submenu::~cUserInterface_Submenu(void)
{
	Clear();
}


void cUserInterface_Submenu::BuildItems(void)
{
	Add(new cOsdItem(hk(tr("Metadata...") ) ) );
	Add(new cOsdItem(hk(tr("Search...") ) ) );
	Add(new cOsdItem(hk(tr("Personal Radio") ) ) );
	Add(new cOsdItem(hk(tr("Neighbours Radio") ) ) );
	Add(new cOsdItem(hk(tr("Loved Tracks Radio"))));
	Add(new cOsdItem(hk(tr("Recommendation Radio"))));
	Add(new cOsdItem(hk(tr("Playlist Radio"))));

	Display();
}


eOSState cUserInterface_Submenu::ProcessKey(eKeys Key)
{
	if (!HasSubMenu())
		switch (Key) {
		case kBack:
			cRemote::CallPlugin("lastfm");
			return osContinue;
			break;
		default:
			break;
		}

	eOSState state = cOsdMenu::ProcessKey(Key);

	if (HasSubMenu())
		return osContinue;

	if (state == osUnknown) {
		switch (Key) {
		case kOk:
			if (!strcmp(Get(Current())->Text(), tr("Search...") ) ) {
				AddSubMenu(new cUserInterface_Search() );
			} else if (!strcmp(Get(Current())->Text(), tr("Metadata...") ) ) {
				AddSubMenu(new cUserInterface_Metadata() );
			} else if (!strcmp(Get(Current())->Text(), tr("Personal Radio") ) ) {
				std::stringstream sstream1;
				sstream1 << "lastfm://user/" << ParameterMap[PARAMETERMAP_USERNAME] << "/personal";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Submenu::ProcessKey: Changestation - personal\r\n");
#endif
				int rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					if (rc == 5) {
						Skins.Message(
							      mtError,
							      tr("Feature only available to subscribers!") );
					}
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");                 // return to mainmenu
				}
			} else if (!strcmp(Get(Current())->Text(), tr("Neighbours Radio") ) ) {
				std::stringstream sstream1;
				sstream1 << "lastfm://user/" << ParameterMap[PARAMETERMAP_USERNAME] << "/neighbours";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Submenu::ProcessKey: Changestation\r\n");
#endif
				int rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					Skins.Message(mtError, tr("Action was not successful!") );
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");                 // return to mainmenu
				}
			} else if (!strcmp(Get(Current())->Text(), tr("Loved Tracks Radio") ) ) {
				std::stringstream sstream1;
				sstream1 << "lastfm://user/" << ParameterMap[PARAMETERMAP_USERNAME] << "/loved";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Submenu::ProcessKey: Changestation\r\n");
#endif
				int rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					if (rc == 5) {
						Skins.Message(mtError,tr("Feature only available to subscribers!") );
					}
					Skins.Message(mtError, tr("Action was not successful!") );
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");                 // return to mainmenu
				}
			} else if (!strcmp(Get(Current())->Text(), tr("Recommendation Radio") ) ) {
				std::stringstream sstream1;
				sstream1 << "lastfm://user/" << ParameterMap[PARAMETERMAP_USERNAME] << "/recommended";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Submenu::ProcessKey: Changestation\r\n");
#endif
				int rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					Skins.Message(mtError, tr("Action was not successful!") );
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");                 // return to mainmenu
				}
			} else if (!strcmp(Get(Current())->Text(), tr("Playlist Radio") ) ) {
				std::stringstream sstream1;
				sstream1 << "lastfm://user/" << ParameterMap[PARAMETERMAP_USERNAME] << "/playlist";
#ifdef LASTFM_DEBUG_WEBSERVICE
				LOGDBG("cUserInterface_Submenu::ProcessKey: Changestation\r\n");
#endif
				int rc = cWebServiceConnector::Changestation(MetadataMap["session"], sstream1.str().c_str() );
				if (rc != 0) {
					Skins.Message(mtError, tr("Action was not successful!") );
				} else {
					cRemote::Put(kNext);
					cRemote::CallPlugin("lastfm");                 // return to mainmenu
				}
			}
#ifdef LASTFM_DEBUG
			LOGDBG("eOSState cUserInterface_Submenu::ProcessKey: switch( Key ): %d\r\n", Key);
#endif
			break;
		default:
			state = osContinue;
			break;
		}
	}

	return state;
}


