/**
 * web_servicecontroller_webservices.c: Static functions, that can be called
 * in order to use last.fm webservice.
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_serviceconnector_webservices.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <sstream>
#include <map>          // postdata
#include "web_serviceconnector_webservices.h"
#include <vdr/tools.h>  // logging
#include "logdefs.h"    // logging
#include "web_networkcontroller.h"
#include "config.h"
#include "utils.h" // md5
#include "ext/xmlParser/xmlParser.h"

extern std::map<std::string, std::string> ParameterMap;

uint8_t cWebServiceConnectorWebservices_auth::getMobileSession(const std::string username,
							   const std::string passphrase_md5,
							   std::string &websvcs_session,
							   std::string &authtoken)
{
	std::stringstream sstream_tmp;
	std::string api_signature;

	sstream_tmp << username << passphrase_md5;
	authtoken = cUtils::MD5(sstream_tmp.str());

			sstream_tmp.str("");
			sstream_tmp.clear();
			sstream_tmp << "api_key" << APIKEY
				    << "authToken" << authtoken
				    << "methodauth.getmobilesession"
				    << "username" << username
				    << APISECRET;
#ifdef LASTFM_DEBUG_WEBSERVICE
			LOGDBG("api_signature: %s\r\n", sstream_tmp.str().c_str());
#endif
			api_signature = cUtils::MD5(sstream_tmp.str());

	std::stringstream url_stream;
	std::string buffer_HttpPostResponse;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["authToken"] = authtoken;
	postdata["method"] = "auth.getmobilesession";
	postdata["username"] = username;
	postdata["api_sig"] = api_signature;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_auth::getMobileSession:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	XMLNode xMainNode, xNode;
	XMLResults xe;

	xMainNode = XMLNode::parseString(buffer_HttpPostResponse.c_str(), "", &xe);

	if (xe.error)
		return 3;

	sstream_tmp.clear();
	sstream_tmp.str("");
	sstream_tmp << xMainNode.getChildNode("lfm").getChildNode("session").getChildNode("key").getText();
	websvcs_session = sstream_tmp.str();
#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_auth::getMobileSession:websvcs_session: %s\r\n", websvcs_session.c_str());
#endif

	if (websvcs_session.empty() || websvcs_session.compare("FAILED") == 0)
		return 1;

	return 0;
}


/**
   <http://www.lastfm.de/api/mobileauth>

   4. Sign your calls

   Construct your api method signatures by first ordering all the parameters sent in your call alphabetically by parameter name and concatenating them into one string using a <name><value> scheme. So for a call to auth.getSession you may have:

   api_keyxxxxxxxxauthTokenxxxxxxxmethodauth.getSession

   Ensure your parameters are utf8 encoded. Now append your secret to this string. Finally, generate an md5 hash of the resulting string. For example, for an account with a secret equal to '3', your api signature will be:

   api signature = md5("api_keyxxxxxxxxauthTokenxxxxxxxmethodauth.getSession3")

   Where md5() is an md5 hashing operation and its argument is the string to be hashed. The hashing operation should return a 32-character hexadecimal md5 hash.
 */
uint8_t cWebServiceConnectorWebservices_track::love(const std::string track, const std::string artist,
						const std::string api_key, const std::string api_sig,
						const std::string sk)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["artist"] = artist;
	postdata["method"] = "track.love";
	postdata["sk"] = sk;
	postdata["track"] = track;
	postdata["api_sig"] = api_sig;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_track::love:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	return 0;
}


uint8_t cWebServiceConnectorWebservices_track::ban(const std::string track, const std::string artist,
					       const std::string api_key, const std::string api_sig,
					       const std::string sk)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["artist"] = artist;
	postdata["method"] = "track.ban";
	postdata["sk"] = sk;
	postdata["track"] = track;
	postdata["api_sig"] = api_sig;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_track::ban:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	return 0;
}


uint8_t cWebServiceConnectorWebservices_artist::getInfo(const std::string artist, const std::string mbid, const std::string lang, const std::string api_key, std::string & content)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["artist"] = artist;
	postdata["lang"] = lang;
	postdata["mbid"] = mbid;
	postdata["method"] = "artist.getInfo";

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_artist::getInfo:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

/**
 * TODO: Will not parse childNode("content") with CDATA
	XMLNode xMainNode, xNode;
	XMLResults xe;

	xMainNode = XMLNode::parseString(buffer_HttpPostResponse.c_str(), "", &xe);

	if (xe.error)
		return 3;

	sstream_tmp.clear();
	sstream_tmp.str("");
	sstream_tmp << xMainNode.getChildNode("lfm").getChildNode("artist").getChildNode("bio").getChildNode("content").getText();
	content = sstream_tmp.str();
 */

	size_t loc = buffer_HttpPostResponse.find("<content><![CDATA");
	content = buffer_HttpPostResponse.substr(loc + 18);

	return 0;
}

uint8_t cWebServiceConnectorWebservices_user::getRecentTracks(std::string & content, const std::string user, const std::string api_key, const uint16_t limit)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	if (limit>=0) {
		postdata["limit"] = limit;
	}
	postdata["method"] = "user.getrecenttracks";
	postdata["user"] = user;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_user::getrecenttracks:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	content = buffer_HttpPostResponse;

	return 0;
}

uint8_t
cWebServiceConnectorWebservices_user::getLovedTracks(std::string & content, const std::string user, const std::string api_key)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["method"] = "user.getlovedtracks";
	postdata["user"] = user;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_user::getLovedTracks:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	content = buffer_HttpPostResponse;

	return 0;
}


uint8_t cWebServiceConnectorWebservices_user::getRecentStations(std::string & content, const std::string user, const std::string api_key, const uint16_t limit)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	if (limit>=0) {
		postdata["limit"] = limit;
	}
	postdata["method"] = "user.getrecentstations";
	postdata["user"] = user;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_user::getrecentstations:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	content = buffer_HttpPostResponse;

	return 0;
}


uint8_t cWebServiceConnectorWebservices_track::updateNowPlaying(const std::string track, const std::string artist,
						const std::string sk)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	std::string api_sig;

			sstream_tmp.str("");
			sstream_tmp.clear();
			sstream_tmp << "api_key" << APIKEY
				    << "artist" << artist
				    << "methodtrack.updateNowPlaying"
				    << "sk" << sk
				    << "track" << track
				    << APISECRET;
			api_sig = cUtils::MD5(sstream_tmp.str());

	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["api_key"] = APIKEY;
	postdata["artist"] = artist;
	postdata["method"] = "track.updateNowPlaying";
	postdata["sk"] = sk;
	postdata["track"] = track;
	postdata["api_sig"] = api_sig;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_track::updateNowPlaying:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	return 0;
}


uint8_t cWebServiceConnectorWebservices_track::scrobble(const std::string track, const std::string timestamp, const std::string artist,
						const std::string sk)
{
	std::string buffer_HttpPostResponse;
	std::stringstream sstream_tmp, url_stream;

	std::map<std::string, std::string> postdata;
	cNetworkController NetworkController;

	std::string api_sig;

			sstream_tmp.str("");
			sstream_tmp.clear();
			sstream_tmp << "api_key" << APIKEY
				    << "artist" << artist
				    << "methodtrack.scrobble"
				    << "sk" << sk
				    << "timestamp" << timestamp // Timestamp when track started to play.
				    << "track" << track
				    << APISECRET;
			api_sig = cUtils::MD5(sstream_tmp.str());


	url_stream << "http://" << AUDIOSCROBBLER_DOMAIN << ":"
		   << AUDIOSCROBBLER_PORT << AUDIOSCROBBLER_BASEPATH_WEBSVCS;

	postdata["track"] = track;
	postdata["timestamp"] = timestamp;
	postdata["api_key"] = APIKEY;
	postdata["artist"] = artist;
	postdata["method"] = "track.scrobble";
	postdata["sk"] = sk;
	postdata["api_sig"] = api_sig;

	if (!NetworkController.HttpPostRequest(buffer_HttpPostResponse,
					       url_stream.str(), postdata) ) {
		return 2;
	}

#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnectorWebservices_track::scrobble:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpPostResponse.c_str());
#endif

	return 0;
}

