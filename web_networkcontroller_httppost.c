/**
 * web_networkcontroller_httppost.c: HTTPPost
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httppost.c 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <sstream>
#include "web_networkcontroller_httppost.h"
#include "config.h" // PLUGIN_NAME PLUGIN_VERSION

My_Web_HttpPostSocket::My_Web_HttpPostSocket(ISocketHandler & h, const std::string & url_in)
	: HttpPostSocket(h, url_in)
{
}


std::string My_Web_HttpPostSocket::MyUseragent()
{
	std::stringstream buff_stream;

	buff_stream << PLUGIN_NAME << "/" << PLUGIN_VERSION;
	std::string buff;
	buff = buff_stream.str();

	return buff;
}


