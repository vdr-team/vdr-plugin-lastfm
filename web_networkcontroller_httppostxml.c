/**
 * web_networkcontroller_httppostxml.c: HTTPPostXML
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httppostxml.c 256 2009-03-31 22:59:57Z hitman_47 $
 */


#include <string.h>
#include <stdlib.h>
#include <sstream>
#include "web_networkcontroller_httppostxml.h"
#include "config.h"             // PLUGIN_NAME PLUGIN_VERSION
#include <Sockets/Utility.h>    /** Utility::l2string */

My_Web_HttpPostXmlSocket::My_Web_HttpPostXmlSocket(ISocketHandler & h,
						   const std::string & url_in, const std::string xmldata) :
	HttpPostSocket(h, url_in), _xmldata(xmldata)
{
}


std::string My_Web_HttpPostXmlSocket::MyUseragent()
{
	std::stringstream buff_stream;

	buff_stream << PLUGIN_NAME << "/"
		    << PLUGIN_VERSION;
	std::string buff;
	buff = buff_stream.str();

	return buff;
}


// with that method, we can force the server to send us no gzipped data:
void My_Web_HttpPostXmlSocket::OnConnect(void)
{
	// build header, send body
	SetMethod("POST");
	SetHttpVersion("HTTP/1.1");
	AddResponseHeader("Host", GetUrlHost() );   // oops - this is actually a request header that we're adding..
	AddResponseHeader("User-agent", MyUseragent() );
	AddResponseHeader("Accept", "text/html, text/plain, */*;q=0.01");
	AddResponseHeader("Connection", "close");
	AddResponseHeader("Content-Type", "text/xml;charset=utf-8");
	AddResponseHeader("Content-length", Utility::l2string( (long)_xmldata.size() ) );
	SendRequest();


	// send body
	Send(_xmldata);
}


