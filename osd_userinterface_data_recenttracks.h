/**
 * osd_userinterface_data_recenttracks.h: Presents recent tracks. 
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-15 13:08:13  mf $
 */

#ifndef __USERINTERFACE_DATA_RECENTTRACKS_H
#define __USERINTERFACE_DATA_RECENTTRACKS_H

#include <vdr/menu.h>

class cUserInterface_Data_recentTracks : public cOsdMenu
{
private:
	void BuildItems();
public:
	cUserInterface_Data_recentTracks(void);
};

#endif //__USERINTERFACE_DATA_RECENTTRACKS_H

