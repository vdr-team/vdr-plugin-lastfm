#
# Makefile for a Video Disk Recorder plugin
#
# $Id$

# The official name of this plugin.
# This name will be used in the '-P...' option of VDR to load the plugin.
# By default the main source file also carries this name.
# IMPORTANT: the presence of this macro is important for the Make.config
# file. So it must be defined, even if it is not used here!
#
PLUGIN = lastfm

### The version number of this plugin (taken from file config.h):

VERSION := $(shell grep 'define PLUGIN_VERSION' config.h | awk '{ print $$3 }' | sed -e 's/[";]//g')
ifeq ($(shell grep 'define PLUGIN_VERSION' config.h | awk '{ print $$3 }' | sed -e 's/[";]//g' | tr -d [:digit:][:punct:]), git)
	VERSION_GIT:=$(VERSION)-$(shell git show --abbrev-commit --pretty=oneline | sed -n '1 {s/ .*//p}' | tr -d '\n')
	DEFINES+=-DPLUGIN_VERSION='"$(VERSION_GIT)"'
	VERSION := $(VERSION_GIT)
endif


### The C++ compiler and options:

CXX      ?= g++
CXXFLAGS ?= -fPIC -g -O2 -Wall -Woverloaded-virtual -Wno-parentheses

### The directory environment:

VDRDIR = ../../..
LIBDIR = ../../lib
TMPDIR = /tmp

### Allow user defined options to overwrite defaults:

-include $(VDRDIR)/Make.config

### The version number of VDR's plugin API (taken from VDR's "config.h"):

APIVERSION = $(shell sed -ne '/define APIVERSION/s/^.*"\(.*\)".*$$/\1/p' $(VDRDIR)/config.h)

### The name of the distribution archive:

ARCHIVE = $(PLUGIN)-$(VERSION)
PACKAGE = vdr-$(ARCHIVE)

### Includes and Defines (add further entries here):

INCLUDES+= -I$(VDRDIR)/include
ifdef LASTFM_HAVE_MAGICK
	INCLUDES += $(shell Magick++-config --cppflags)
	# needed since imagemagick 6.4
	INCLUDES += $(shell MagickCore-config --cppflags)
	INCLUDES += $(shell Magick++-config --cppflags)
endif

DEFINES+= -D_GNU_SOURCE -DPLUGIN_NAME_I18N='"$(PLUGIN)"'

ifdef LASTFM_DEBUG
  CXXFLAGS=-fPIC -g3 -ggdb -O0 -Wall -Woverloaded-virtual -Wno-parentheses
  DEFINES+=-DLASTFM_DEBUG

  ifdef LASTFM_DEBUG_AUDIO
    DEFINES+=-DLASTFM_DEBUG_AUDIO
  endif
  ifdef LASTFM_DEBUG_AUDIO_HTTP
    DEFINES+=-DLASTFM_DEBUG_AUDIO_HTTP
  endif
  ifdef LASTFM_DEBUG_WEBSERVICE
    DEFINES+=-DLASTFM_DEBUG_WEBSERVICE
  endif
endif
ifdef LASTFM_HAVE_MAGICK
    DEFINES += -DLASTFM_HAVE_MAGICK
    ifdef LASTFM_DEBUG_MAGICK
	DEFINES+=-DLASTFM_DEBUG_MAGICK
    endif
endif
ifdef LASTFM_HAVE_GRAPHTFT
  DEFINES += -DLASTFM_HAVE_GRAPHTFT
  ifdef LASTFM_DEBUG_GRAPHTFT
    DEFINES+=-DLASTFM_DEBUG_GRAPHTFT
  endif
endif

### The object files (add further files here):

OBJS = $(PLUGIN).o
OBJS+= profile.o osd_userinterface_profileselection.o
OBJS+= audio_buffer.o \
       audio_decoder_mp3_http.o audio_decoder_mp3.o \
       audio_control.o \
       audio_player.o audio_filter.o
OBJS+= playlist.o
OBJS+= web_serviceconnector.o \
       web_networkcontroller.o web_networkcontroller_httpget.o \
       web_networkcontroller_httppost.o web_networkcontroller_httppostxml.o \
       web_serviceconnector_webservices.o
OBJS+= utils.o md5.o
OBJS+= osd_osdtools.o \
       osd_userinterface_submenu.o \
       osd_userinterface_search.o osd_userinterface_search_favourites.o \
       osd_userinterface_metadata.o osd_userinterface_data_songLyrics.o osd_userinterface_data_artistMetadata.o \
       osd_userinterface_data_recenttracks.o osd_userinterface_data_getlovedtracks.o
OBJS+= setup.o setup_parameters.o setup_favourites.o \
       settings_xml.o
ifdef LASTFM_HAVE_MAGICK
	OBJS+= image_bitmap.o image_quantize.o
endif

LIBS = -lmad
LIBS+= -lSockets
# if libsockets has been built with SSL support, we need to link SSL library here:
OPENSSL=$(shell Sockets-config -info|grep -o SSL)
ifeq ($(OPENSSL), SSL)
LIBS+= -lssl
endif
LIBS     += ext/xmlParser/xmlParser.o
ifdef LASTFM_HAVE_MAGICK
	LIBS += $(shell Magick++-config --libs)
	# needed since imagemagick 6.4:
	LIBS += $(shell MagickCore-config --libs)
	LIBS += $(shell Magick++-config --libs)
endif

### The main target:

all: libvdr-$(PLUGIN).so i18n

### Implicit rules:

%.o: %.c
	$(CXX) $(CXXFLAGS) -c $(DEFINES) $(INCLUDES) $<

### Dependencies:

MAKEDEP = $(CXX) -MM -MG
DEPFILE = .dependencies
$(DEPFILE): Makefile
	@$(MAKEDEP) $(DEFINES) $(INCLUDES) $(OBJS:%.o=%.c) > $@

-include $(DEPFILE)

### Internationalization (I18N):

PODIR     = po
LOCALEDIR = $(VDRDIR)/locale
I18Npo    = $(wildcard $(PODIR)/*.po)
I18Nmsgs  = $(addprefix $(LOCALEDIR)/, $(addsuffix /LC_MESSAGES/vdr-$(PLUGIN).mo, $(notdir $(foreach file, $(I18Npo), $(basename $(file))))))
I18Npot   = $(PODIR)/$(PLUGIN).pot

%.mo: %.po
	msgfmt -c -o $@ $<

$(I18Npot): $(wildcard *.c)
	xgettext -C -cTRANSLATORS --no-wrap --no-location -k -ktr -ktrNOOP --msgid-bugs-address='<see README>' -o $@ $^

%.po: $(I18Npot)
	msgmerge -U --no-wrap --no-location --backup=none -q $@ $<
	@touch $@

$(I18Nmsgs): $(LOCALEDIR)/%/LC_MESSAGES/vdr-$(PLUGIN).mo: $(PODIR)/%.mo
	@mkdir -p $(dir $@)
	cp $< $@

.PHONY: i18n
i18n: $(I18Nmsgs) $(I18Npot)

### Targets:

xmlParser:
	$(MAKE) -C ext/xmlParser CXX="$(CXX)" CXXFLAGS="$(CXXFLAGS)"
	
libvdr-$(PLUGIN).so: $(OBJS) xmlParser Makefile
	$(CXX) $(CXXFLAGS) -shared $(OBJS) $(LIBS) -o $@
	@cp --remove-destination $@ $(LIBDIR)/$@.$(APIVERSION)

dist: clean
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@mkdir $(TMPDIR)/$(ARCHIVE)
	@cp -a * $(TMPDIR)/$(ARCHIVE)
	@tar czf $(PACKAGE).tgz -C $(TMPDIR) $(ARCHIVE)
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@echo Distribution package created as $(PACKAGE).tgz

clean:
	@-rm -f $(OBJS) $(DEPFILE) *.so *.tgz core* *~ $(PODIR)/*.mo $(PODIR)/*.pot
	$(MAKE) -C ext/xmlParser clean ;

