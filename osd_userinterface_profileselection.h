/**
 * osd_userinterface_profileselections.h: Presents profiles to be selected
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: osd_userinterface_profileselection.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __USERINTERFACE_PROFILESELECTION_H
#define __USERINTERFACE_PROFILESELECTION_H

#include <vdr/menu.h>
#include <string>
#include <list>

class cUserInterface_Profileselection : public cOsdMenu
{
public:
	cUserInterface_Profileselection(std::list<std::string> profilenames);
private:
	void BuildItems(void);
	virtual eOSState ProcessKey(eKeys Key);
	std::list<std::string> m_profilenames;
};

#endif //__USERINTERFACE_PROFILESELECTION_H
