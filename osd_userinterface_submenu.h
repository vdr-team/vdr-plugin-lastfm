/**
 * osd_userinterface_submenu.h: Ssubmenu for several items
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: osd_userinterface_submenu.h 256 2009-03-31 22:59:57Z hitman_47 $
 */


#ifndef __USERINTERFACE_SUBMENU_H
#define __USERINTERFACE_SUBMENU_H

#include <vdr/menu.h>
#include "web_serviceconnector.h"

class cUserInterface_Submenu : public cOsdMenu
{
private:
	void BuildItems(void);
public:
	cUserInterface_Submenu(void);
	~cUserInterface_Submenu(void);
	virtual eOSState ProcessKey(eKeys Key);
};

#endif //__USERINTERFACE_SUBMENU_H
