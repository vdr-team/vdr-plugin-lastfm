/**
 * osd_userinterface_data_songLyrics.h: Presents lyrics of the current song
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2010-01-31 19:53:22  mf $
 */

#include "osd_userinterface_data_songLyrics.h"
#include <sstream>      /** url_stream */
#include "utils.h"      /** rfc1738encode */
#include "web_networkcontroller.h"
#include <vdr/tools.h>
#include "logdefs.h"


extern std::map<std::string, std::string> MetadataMap;

cUserInterface_Data_songLyrics::cUserInterface_Data_songLyrics(void) :
	cMenuText("", "")
{
	// OSD-Elemente aufbauen
	BuildItems();
}


void cUserInterface_Data_songLyrics::BuildItems(void)
{
	cNetworkController net;
	std::string buffer_HttpGetRequest;
	std::stringstream url_stream, sstream_tmp;
	std::string artist, track;

	std::string response("no lyrics found");    /** at first, assume there is no data */


	/** harvest the now playing metadata */
	artist = MetadataMap[ "track_creator" ];
	track = MetadataMap[ "track_title" ];

	/** get that data from lyricwiki */
	url_stream << "http://lyrics.wikia.com/api.php?func=getSong&fmt=text" <<
	"&artist=" << cUtils::rfc1738_encode(artist) <<             /** before sendine request, tagstrings will be converted for rfc1738 compliancy */
	"&song=" << cUtils::rfc1738_encode(track);

	net.HttpGetRequest(buffer_HttpGetRequest, url_stream.str() );
#ifdef LASTFM_DEBUG_WEBSERVICE
	LOGDBG("cWebServiceConnector::Changestation:NetworkController.HttpGetRequest: %s\r\n", buffer_HttpGetRequest.c_str());
#endif

	/** Now, show the data on OSD: */
	sstream_tmp.str("");
	sstream_tmp.clear();
	sstream_tmp << artist << " - " << track;
	SetTitle(sstream_tmp.str().c_str());

	sstream_tmp.str("");
	sstream_tmp.clear();
	sstream_tmp << buffer_HttpGetRequest.c_str() << std::endl << std::endl << tr("Lyrics come from http://lyrics.wikia.com/");
	SetText(sstream_tmp.str().c_str());

	Display();
}


