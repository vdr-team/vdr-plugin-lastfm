/**
 * utils.h: Class delivers some static helper functions
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: utils.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#include <string>

class cUtils
{
public:
	static bool compare_nocase(const std::string x, const std::string y);
	/**
	 * rfc1738_encode
	 * Encode string per RFC1738 URL encoding rules
	 * tnx rstaveley
	 */
	static std::string rfc1738_encode(const std::string & src);  // from Sockets; Utility.h (www.alhem.net/Sockets/)
	static std::string MD5(const std::string & input);
	static const char * GetFileNameChars_lastfm();
	static int html2text(std::string & output, const std::string input);
};
