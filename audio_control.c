/*
 * audio_control.c: The control interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2010-11-19 20:31:55  mf $
 */

#ifdef LASTFM_HAVE_GRAPHTFT
 #include <vdr/plugin.h>
 #include "../graphtft/common.h" // cmdUnknown = na
 #include "../graphtft/service.h"
#endif
#include <sstream>
#include "audio_control.h"
#include "config.h" // MAXIMUM_THREADKILLTIME
#include "osd_userinterface_profileselection.h"
#include <vdr/status.h>
#include <vdr/tools.h>
#include "logdefs.h"
#ifdef LASTFM_HAVE_MAGICK
 #include "image_bitmap.h"
 #include "web_serviceconnector_webservices.h"
#endif // LASTFM_HAVE_MAGICK


extern std::map<std::string, std::string> ParameterMap;

cAudioControl::cAudioControl(void)
	: cControl(m_player = new cAudioPlayer)
{
	m_OsdTools = 0;
	m_UserInterface_Submenu = 0;
	m_osd = 0;
	m_displayMenu = 0;
	m_visible = 0;
	// Osd_Tools:
	m_Osd_Width = 480;
	m_Osd_Height = 360;
	m_Osd_AreaDepth = 4;
	m_Osd_ColourDepth = 16;
	m_Osd_Centre_x = m_Osd_Width / 2;
	m_Osd_Centre_y = m_Osd_Height / 2;
	m_Osd_BitmapSize_x = 180;
	m_Osd_BitmapSize_y = 180;
	m_Osd_ImgAlpha = 255;
	m_Osd_BtnWidth = m_Osd_Width / 4;
	m_Osd_FontHeight = 20;

	ParameterMap["running"] = "1";

	m_currentmenuitem = 0;

/////  cStatus::MsgReplaying(this,"[music] MP3",0, 1);
}


cAudioControl::~cAudioControl()
{
	if (m_visible)
		Hide();

	if (m_player)
		DELETENULL(m_player);

	ParameterMap.clear(); // delete currentprofilename, so a fresh start of the plugin will not use old profile.

/////  cStatus::MsgReplaying(this, 0, NULL, false);
}


void cAudioControl::Show(void)
{
	if (!m_visible) {
#ifdef LASTFM_DEBUG
		LOGDBG("cAudioControl::Show:ParameterMap[\"Osd_Mode\"]: %s\r\n", ParameterMap[PARAMETERMAP_OSD_MODE].c_str());
#endif
		SetOsd();
		SetTitle();
		SetHelp();
		m_visible = true;

		BuildMenu();

		// Thread starten, der die MetaDaten aktualisiert, wie vom Benutzer im Setup angegeben
		if (atoi(ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL].c_str() ) ) {
			Start(); // start thread, that will refresh main OSD info (trackinfo)
		}
	}
}


void cAudioControl::Hide(void)
{
	Cancel(MAXIMUM_THREADKILLTIME);                       // OSD update thread

	if (m_displayMenu)              // textbased
		DELETENULL(m_displayMenu);

	cStatus::MsgOsdClear();

	if (m_osd)
		DELETENULL(m_osd);

	if (m_OsdTools)
		DELETENULL(m_OsdTools);

	m_visible = 0;

	if (m_UserInterface_Submenu)
		DELETENULL(m_UserInterface_Submenu);
}


eOSState cAudioControl::ProcessKey(eKeys Key)
{
	std::stringstream sstream_tmp;

	eOSState state = osContinue;

	if (!m_player->Active()) {
		return osEnd;
	}


	/** This way, external functions can trigger next without knowing address of m_player. */
	if (Key == kNext) {
		m_player->Next();
	}

	if (m_UserInterface_Submenu) {
		// give the control to our sub-menu
		return m_UserInterface_Submenu->ProcessKey(Key);
	}

	switch (Key & ~k_Repeat) {
	case kRed:
		m_player->Submit(1); // Love
		break;
	case kGreen:
		m_player->Next();
		break;
	case kYellow:
		m_player->Submit(2);    // Ban
		m_player->Next();       // Skip after ban immediately
		break;
	case kBlue:
		if (m_visible) {
			Hide();
		}
		state = osEnd;
		break;
	case kOk:
		if (m_visible) {
			Hide();
		} else
			Show();
		break;
	case kBack:
		if (m_visible) {
			Hide();
		} else
			Show();
		break;
	case k5:
		if (m_visible) {
			Hide();
		}
		m_UserInterface_Submenu = new cUserInterface_Submenu();
		m_visible = 1;
	case kUp:
		if (m_currentmenuitem > 0)
			m_currentmenuitem--;

		cStatus::MsgOsdCurrentItem(m_menuitems.at(m_currentmenuitem).c_str());
		break;
	case kDown:
#ifdef LASTFM_DEBUG
		LOGDBG("kDown: %d\r\n", m_menuitems.size() );
#endif
		if (m_currentmenuitem < m_menuitems.size() - 1)
			m_currentmenuitem++;

		cStatus::MsgOsdCurrentItem(m_menuitems.at(m_currentmenuitem).c_str());
		break;
	case k0:
		BuildMenu();
		break;
	default:
		state = osContinue;
		break;
	}

	return state;
}


void cAudioControl::BuildMenu(void)
{
	if (!m_visible)
		return;

	std::string creator, title, album, image;
	std::stringstream sstream_tmp;

	if (!m_player->m_trackList.empty()) {
		// Titelanzeige (oben rechts)
		// artist
		sstream_tmp.str("");
		sstream_tmp.clear();
		sstream_tmp << m_player->m_trackList.front().begin()->second["track_creator"];
		creator = sstream_tmp.str();
		// track
		sstream_tmp.str("");
		sstream_tmp.clear();
		sstream_tmp << m_player->m_trackList.front().begin()->second["track_title"];
		title = sstream_tmp.str();
		// album
		sstream_tmp.str("");
		sstream_tmp.clear();
		sstream_tmp << m_player->m_trackList.front().begin()->second["track_album"];
		album = sstream_tmp.str();
		// image
		sstream_tmp.str("");
		sstream_tmp.clear();
		sstream_tmp << m_player->m_trackList.front().begin()->second["track_image"];
		image = sstream_tmp.str();
	}

	const char * seperator = "\t";
	m_menuitems.clear(); // clear menu entries before inserting our three items

	// creator
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0 && m_osd && m_OsdTools) {        // graphical OSD
		AddItem(creator.c_str());
	} else if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("1") == 0) {                        // textual OSD
		sstream_tmp.clear();                                                    // clear resets bits only
		sstream_tmp.str("");                                                    // assign empty string before further inputs
		sstream_tmp << tr("artist") << seperator << creator;
		AddItem(sstream_tmp.str());
	}

	// title
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0 && m_osd && m_OsdTools) {        // graphical OSD
		AddItem(title.c_str());
	} else if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("1") == 0) {                        // textual OSD
		sstream_tmp.clear();                                                    // clear resets bits only
		sstream_tmp.str("");                                                    // assign empty string before further inputs
		sstream_tmp << tr("track") << seperator << title;
		AddItem(sstream_tmp.str());
	}

	// album
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0 && m_osd && m_OsdTools) {        // graphical OSD
		AddItem(album.c_str());
	} else if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("1") == 0) {                        // textual OSD
		sstream_tmp.clear();                                                    // clear resets bits only
		sstream_tmp.str("");                                                    // assign empty string before further inputs
		sstream_tmp << tr("album") << seperator << album;
		AddItem(sstream_tmp.str());
	}
	// albumcover links, mittig
#ifdef LASTFM_HAVE_MAGICK
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0 && m_osd && m_OsdTools) { // graphical OSD

#ifdef LASTFM_DEBUG_MAGICK
	LOGDBG("cAudioControl::BuildMenu:image: %s\r\n", image.c_str());
#endif
		if (!image.empty() &&
		    atoi(ParameterMap[PARAMETERMAP_ALBUMCOVERMODE].c_str() ) ) {
cMP3Bitmap * bmp;
			if ( ( bmp = cMP3Bitmap::Load(image,
						      m_Osd_ImgAlpha,
						      m_Osd_BitmapSize_x - 1,
						      m_Osd_BitmapSize_y - 1,
						      m_Osd_ColourDepth) ) != 0) {
				if (m_osd->GetBitmap(1) )
					m_osd->GetBitmap(1)->Reset();

				m_osd->DrawBitmap(0,
						  m_Osd_Centre_y - m_Osd_BitmapSize_y / 2,
						  bmp->Get() );

				m_osd->Flush();
			}
		}
	}
#endif          // LASTFM_HAVE_MAGICK
}


void cAudioControl::Action(void)
{
	int c = 0; // summarize waitseconds up to our Osd_Refreshinterval, so
	// we have only waitstates of 10ms and Cancel is not that hard
	int c_max = atoi(ParameterMap[PARAMETERMAP_OSD_REFRESHINTERVAL].c_str()) * 1000;
	int mswaitstate = 10;

	while (Running()) {
		cCondWait::SleepMs(mswaitstate);

		if (c_max <= c) {
			BuildMenu();
			c = 0;
		}

		c = c + mswaitstate;
	}
}


#ifdef LASTFM_HAVE_GRAPHTFT
void cAudioControl::SendGtftHelpButtons(const char * red, const char * green, const char * yellow, const char * blue)
{
	// transmit help buttons to graphtft
	if (cPluginManager::GetPlugin("graphtft")) {
		cGraphTftComService::MusicServiceHelpButtons buttons;
		buttons.red = red;
		buttons.green = green;
		buttons.yellow = yellow;
		buttons.blue = blue;

		cPluginManager::CallFirstService(GRAPHTFT_HELPBUTTONS_ID, &buttons);
 #ifdef LASTFM_DEBUG_GRAPHTFT
		LOGDBG("transmission of help buttons succeeded (gtft)\r\n");
 #endif
/**		
		cGraphTftComService::MusicServicePlayerInfo gtftStatus;
		// GraphTFT stuff
		gtftStatus.filename = "mode->Filename";
		gtftStatus.artist = "mode->Artist";
		gtftStatus.album = "mode->Album";
		gtftStatus.genre = "mode->Genre";
		gtftStatus.year = 2009;
		gtftStatus.comment = "mode->Comment";
		gtftStatus.frequence = 44100;
		gtftStatus.bitrate = 16;
		gtftStatus.smode = "mode->SMode";
		gtftStatus.index = 0;
		gtftStatus.count = 1;
		gtftStatus.status = "";
		gtftStatus.currentTrack = "mode->Title";
		gtftStatus.loop = "mode->Loop";
		gtftStatus.shuffle = "mode->Shuffle";
		gtftStatus.shutdown = "MP3Setup.EnableShutDown";
		gtftStatus.recording = "MP3Setup.RecordStream";
		gtftStatus.rating = 5;

		cPluginManager::CallFirstService(GRAPHTFT_STATUS_ID, &gtftStatus);
*/
	}
}


#endif
void cAudioControl::SetHelp(void)
{
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0) {       // graphical OSD
		if (m_OsdTools && m_osd) {
			m_OsdTools->SetHelpButtons();
			m_osd->Flush();
		}
	} else {
		if (m_displayMenu) {
			m_displayMenu->SetButtons(tr("Love"), tr("Skip"), tr("Ban"), tr("Exit"));
			m_displayMenu->Flush();
		}
	}

	cStatus::MsgOsdHelpKeys(tr("Love"), tr("Skip"), tr("Ban"), tr("Exit"));
#ifdef LASTFM_HAVE_GRAPHTFT
	SendGtftHelpButtons(tr("Love"), tr("Skip"), tr("Ban"), tr("Exit"));
#endif
}


void cAudioControl::SetTitle(void)
{
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0) {       // graphical OSD
		if (m_OsdTools && m_osd) {
			// Fenstertitel (oben links)
			m_OsdTools->DrawButton(0, 0, "Lastfm", clrBlack);
			m_osd->Flush();
		}
	} else {
		if (m_displayMenu) {
			m_displayMenu->SetTitle(tr("Lastfm"));
			m_displayMenu->Flush();
		}
	}
	cStatus::MsgOsdTitle(tr("Lastfm"));
}


void cAudioControl::SetOsd(void)
{
	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0) {               // graphical OSD
		m_osd = cOsdProvider::NewOsd( ( (Setup.OSDWidth - m_Osd_Width) / 2)
					     + Setup.OSDLeft + 0,       //diese  0  ist der X-Offset
					     ((Setup.OSDHeight - m_Osd_Height) / 2) + Setup.OSDTop);

		if (m_osd) {
			tArea
			Area[] =
			{
				/// Area fuer Titelzeile
				{ 0,                  0,                                                              m_Osd_Width - 1,                                                              (m_Osd_Centre_y
																								     - m_Osd_BitmapSize_y / 2) - 1, 2 },
				/// Area fuer Albumcover
				{ 0,                  (m_Osd_Centre_y - m_Osd_BitmapSize_y / 2),
				  m_Osd_BitmapSize_x - 1, (m_Osd_Centre_y
							   + m_Osd_BitmapSize_y / 2),
				  m_Osd_AreaDepth * 2 },
				/// Area fuer Metadata
				{ m_Osd_BitmapSize_x, m_Osd_Centre_y
				  - m_Osd_BitmapSize_y / 2, m_Osd_Width - 1,
				  m_Osd_BitmapSize_y + m_Osd_BitmapSize_y / 2, 2 },
				/// Area fuer SetHelp buttons
				{ 0,                  (m_Osd_Centre_y + m_Osd_BitmapSize_y / 2) + 1,
				  m_Osd_Width - 1, m_Osd_Height - 1, 4 }
			};

			eOsdError result = m_osd->CanHandleAreas(Area, sizeof(Area) / sizeof(tArea));
			if (result == oeOk) {
				m_osd->SetAreas(Area, sizeof(Area) / sizeof(tArea));
			} else {
				const char * errormsg = 0;
				switch (result) {
				case oeTooManyAreas:
					errormsg = "Too many OSD areas";
					break;
				case oeTooManyColors:
					errormsg = "Too many colors";
					break;
				case oeBppNotSupported:
					errormsg = "Depth not supported";
					break;
				case oeAreasOverlap:
					errormsg = "Areas are overlapped";
					break;
				case oeWrongAlignment:
					errormsg = "Areas not correctly aligned";         /// Beachte: bei FF-Karten wird die Datei dvbosd.c anstatt osd.c benutzt; andere Berechnungsformel
					break;
				case oeOutOfMemory:
					errormsg = "OSD memory overflow";
					break;
				case oeUnknown:
					errormsg = "Unknown OSD error";
					break;
				default:
					break;
				}
				esyslog("lastfm: ERROR! OSD open failed! Can't handle areas (%d)-%s\n", result, errormsg);
				return;
			}

			// Rechteck zeichnen, das das OSD transparent macht
			m_osd->DrawRectangle(m_Osd_Width / 4 - 1 - 1,
					     0,
					     3 * m_Osd_Width / 4 - 1,
					     m_Osd_Height - 1,
					     clrTransparent);

			// Eigene OSD-Funktionen (DrawButton, BevelButton etc.)
			if (m_OsdTools) {
				DELETENULL(m_OsdTools);
			}
			m_OsdTools = new cOsdTools(m_osd);

			m_osd->Flush();
		}
	}   else if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("1") == 0) {                       // textbased
		m_displayMenu = Skins.Current()->DisplayMenu();
		m_displayMenu->SetTabs(12, 40);

		m_displayMenu->Flush();
	}
}


void cAudioControl::AddItem(const std::string itemtext)
{
	unsigned int index = 0;

	m_menuitems.push_back(itemtext);

	if (!m_menuitems.empty())
		index = m_menuitems.size() - 1;

	if (ParameterMap[PARAMETERMAP_OSD_MODE].compare("0") == 0) { // graphical OSD
		if (m_OsdTools && m_osd) {
			m_OsdTools->DrawTextfield(m_Osd_Centre_x, m_Osd_Centre_y - 80 + (index * 40), itemtext.c_str(), clrWhite);
			m_osd->Flush();
		}
	} else {
		if (m_displayMenu) {
			m_displayMenu->SetItem(itemtext.c_str(), index, 0, 0);
			m_displayMenu->Flush();
		}
	}

	cStatus::MsgOsdItem(itemtext.c_str(), index);
	cStatus::MsgOsdCurrentItem(itemtext.c_str());
}


