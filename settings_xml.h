/**
 * settings_xml.h: Stores and loads parameters from users to/from XML file
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: settings_xml.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __SETTINGS_XML_H
#define __SETTINGS_XML_H

#include <string>
#include <list>
#include <map>

static std::string m_configfileurl;
class cXmlconfig
{
public:
	static std::string LoadXmlfile(const std::string filename);
	static int Load(const std::string profilename, std::map<std::string, std::string> & ParameterMap,
			std::list<std::string> & FavouriteList);
	static int Save(const std::string profilename, std::map<std::string, std::string> ParameterMap,
			std::list<std::string> FavouriteList);
	static std::list<std::string> GetProfilenames();
	static int DeleteProfile(const std::string profilename);

	static int SetConfigfileurl(std::string);
};

#endif // __SETTINGS_XML_H
