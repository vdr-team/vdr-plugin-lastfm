/**
 * web_networkcontroller.h: Get,Post,PostXML-Networkmethods
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef WEB_NETWORKCONTROLLER_H_
#define WEB_NETWORKCONTROLLER_H_

#include <string.h>
#include <stdlib.h>
#include <Sockets/SocketHandler.h>

class cNetworkController
{
public:
	bool HttpPostRequest(std::string & body, const std::string url,
			     const std::map<std::string, std::string> postdataMap);
	bool HttpGetRequest(std::string & body, const std::string url);
	bool HttpPostXmlRequest(std::string & body, const std::string url,
				const std::string xmldata);
};

#endif // WEB_NETWORKCONTROLLER_H_
