/**
 * osd_userinterface_search_favourites.c: Search submenu for selecting favourites
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-03-14 14:19:37  mf $
 */

#include <list>
#include <vdr/remote.h>
#include <sstream>
#include "osd_userinterface_search_favourites.h"
#include <vector>
#include "config.h"
#include "logdefs.h"
#include <vdr/tools.h>

extern std::list<std::string> FavouriteList;
cUserInterface_Search_Favourites::cUserInterface_Search_Favourites(char * searchword_ptr)
	: cOsdMenu(tr("SearchFavourites"), 20)
	, m_searchword_ptr(searchword_ptr)
{
	BuildItems();
}


void cUserInterface_Search_Favourites::BuildItems(void)
{
	for (std::list<std::string>::iterator it = FavouriteList.begin();
	     it != FavouriteList.end();
	     it++) {
		std::string it_string = *it;

		Add(new cOsdItem(it_string.c_str() ) );

	}

	Display();
}


eOSState cUserInterface_Search_Favourites::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);

	if (state == osUnknown) {
		switch (Key) {
		case kOk:       // select favourite; execute only, if an item is available
			if (!Get(Current() ) ) {
				Skins.Message(mtError, tr("no favourites entered in setup") );
			} else {
				snprintf(m_searchword_ptr, SEARCHWORD_MAXSIZE, Get(Current() )->Text() );
				return osBack;
			}
			break;
		default:
			state = osContinue;
			break;
		}
	}
	return state;
}


