/**
 * profile.h: Profile
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: profile.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __PROFILE_H
#define __PROFILE_H

#include <string>
#include "playlist.h"
#include <vdr/skins.h>
class cProfile : public cPlaylist
{
public:
	cProfile(void);
	~cProfile();
private:
	int m_running;
	std::string m_username;
	std::string m_passphrase_md5;
	std::string m_session;
	int m_subscriber;
	std::string m_data_npurl, m_data_protourl, m_data_sessionid;
	std::string m_session_websvcs;
	std::string m_authtoken;
	cSkinDisplayMenu * displayMenu;
public:
	int Initialize(void);
	std::string m_profilerunning;
	int FeedPlaylist(void);
	std::string GetNextsongurl(void);
	int Submit(int = 0, int = 0);
	int NowPlaying(void);
};

#endif //__PROFILE_H
