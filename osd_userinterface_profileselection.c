/**
 * osd_userinterface_profileselections.c: Presents profiles to be selected
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-16 13:06:33  mf $
 */

#include "osd_userinterface_profileselection.h"
#include <vdr/remote.h> /** cRemote::CallPlugin */
#include <map>
#include <vdr/tools.h>
#include "logdefs.h"
#include "config.h"

extern std::map<std::string, std::string> ParameterMap;

cUserInterface_Profileselection::cUserInterface_Profileselection(std::list<std::string> profilenames)
	: cOsdMenu(tr("profileselection"))
	, m_profilenames(profilenames)
{
	BuildItems();
}


void cUserInterface_Profileselection::BuildItems(void)
{
	Clear();
	// Profileselection (or creation of a new profile):
	for (std::list<std::string>::iterator it = m_profilenames.begin(); it != m_profilenames.end(); it++) {
		std::string it_string = *it;
		if (!it_string.empty())
			Add(new cOsdItem(it_string.c_str()));
	}
}


eOSState cUserInterface_Profileselection::ProcessKey(eKeys Key)
{
	eOSState state = cOsdMenu::ProcessKey(Key);

	if (state == osUnknown) {
		switch (Key) {
		case kOk:
			ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD] = Get(Current())->Text();
			cRemote::CallPlugin("lastfm");
			break;

		default:
			break;
		}
	}

	return state;
}


