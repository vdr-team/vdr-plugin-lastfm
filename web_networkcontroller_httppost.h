/**
 * web_networkcontroller_httppost.h: HTTPPost
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httppost.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __WEB_NETWORKCONTROLLER_HTTPPOST_H
#define __WEB_NETWORKCONTROLLER_HTTPPOST_H

#include <string.h>
#include <stdlib.h>
#include <Sockets/HttpPostSocket.h>

class My_Web_HttpPostSocket : public HttpPostSocket
{
public:
	My_Web_HttpPostSocket(ISocketHandler &, const std::string & url_in);
private:
	virtual std::string MyUseragent();
};

#endif // __WEB_NETWORKCONTROLLER_HTTPPOST_H
