/*
 * lastfm.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2010-11-19 20:25:24  mf $
 */

#include <vdr/plugin.h>
#include <string>
#include <map>
#include <vdr/tools.h>
#include "logdefs.h"
#include "audio_control.h"
#include "setup.h"
#include "settings_xml.h"
#include "osd_userinterface_profileselection.h"
// Service:
#include "service.h"                            // submission-serviceconnector
#include "web_serviceconnector_webservices.h"     // giving static Submission() to other plugins

static const char * VERSION        = PLUGIN_VERSION;
static const char * DESCRIPTION    = "A player for the service of last.fm";
static const char * MAINMENUENTRY  = "Lastfm";

std::map<std::string, std::string> ParameterMap;
std::list<std::string> FavouriteList;
std::map<std::string, std::string> MetadataMap;

class cPluginLastfm : public cPlugin {
	private:
		// Add any member variables or functions you may need here.
	public:
		cPluginLastfm(void);
		virtual ~cPluginLastfm();
		virtual const char * Version(void)
		{
			return VERSION;
		}


		virtual const char * Description(void)
		{
			return tr(DESCRIPTION);
		}


		virtual const char * CommandLineHelp(void);
		virtual bool ProcessArgs(int argc, char * argv[]);
		virtual bool Initialize(void);
		virtual bool Start(void);
		virtual void Stop(void);
		virtual void Housekeeping(void);
		virtual void MainThreadHook(void);
		virtual cString Active(void);
		virtual time_t WakeupTime(void);
		virtual const char * MainMenuEntry(void)
		{
			return MAINMENUENTRY;
		}


		virtual cOsdObject * MainMenuAction(void);
		virtual cMenuSetupPage * SetupMenu(void);
		virtual bool SetupParse(const char * Name, const char * Value);
		virtual bool Service(const char * Id, void * Data = NULL);
		virtual const char * * SVDRPHelpPages(void);
		virtual cString SVDRPCommand(const char * Command, const char * Option, int & ReplyCode);
};

cPluginLastfm::cPluginLastfm(void)
{
	// Initialize any member variables here.
	// DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
	// VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
}


cPluginLastfm::~cPluginLastfm()
{
	// Clean up after yourself!
}


const char * cPluginLastfm::CommandLineHelp(void)
{
	// Return a string that describes all known command line options.
	return NULL;
}


bool cPluginLastfm::ProcessArgs(int argc, char * argv[])
{
	// Implement command line argument processing here if applicable.
	return true;
}


bool cPluginLastfm::Initialize(void)
{
	// Initialize any background activities the plugin shall perform.
	std::string configDir = ConfigDirectory(PLUGIN_NAME_I18N);

	if (!configDir.empty()) {
		cXmlconfig::SetConfigfileurl(std::string(configDir) + "/lastfm_config.xml");
	}

	return true;
}


bool cPluginLastfm::Start(void)
{
	// Start any background activities the plugin shall perform.
	return true;
}


void cPluginLastfm::Stop(void)
{
	// Stop any background activities the plugin is performing.
}


void cPluginLastfm::Housekeeping(void)
{
	// Perform any cleanup or other regular tasks.
}


void cPluginLastfm::MainThreadHook(void)
{
	// Perform actions in the context of the main program thread.
	// WARNING: Use with great care - see PLUGINS.html!
}


cString cPluginLastfm::Active(void)
{
	// Return a message string if shutdown should be postponed
	return NULL;
}


time_t cPluginLastfm::WakeupTime(void)
{
	// Return custom wakeup time for shutdown script
	return 0;
}


cOsdObject * cPluginLastfm::MainMenuAction(void)
{
#ifdef LASTFM_DEBUG
	LOGDBG("cPluginLastfm::MainMenuAction:running: %s\r\n", ParameterMap[PARAMETERMAP_RUNNING].c_str());
	LOGDBG("cPluginLastfm::MainMenuAction:ProfilenameToLoad: %s\r\n", ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD].c_str());
#endif

	if (!ParameterMap[PARAMETERMAP_RUNNING].empty() || ParameterMap[PARAMETERMAP_RUNNING].compare("1") == 0) {
		cRemote::Put(kOk, 1);
		return 0;
	}

	// not running and no CurrentProfilename:
	if (ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD].empty() ) {
		// Profileselection (or creation of a new profile):
		std::list<std::string> profilenames = cXmlconfig::GetProfilenames();

		// if more than one profile exists, let the user decide:
		if (profilenames.size() > 1)
			return new cUserInterface_Profileselection(profilenames);
		// we load the first profile, if there is only one there
		else if (profilenames.size() == 1) {
			ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD] = profilenames.front();
		}
#ifdef LASTFM_DEBUG
	LOGDBG("cPluginLastfm::MainMenuAction: %s\r\n", ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD].c_str());
#endif
	}

	if (!ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD].empty()) {
		std::string profilenametoload(ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD]);

		if (cXmlconfig::Load(profilenametoload, ParameterMap, FavouriteList)) {
			Skins.Message(mtError, tr("Profile not found!") );
			ParameterMap[PARAMETERMAP_PROFILENAMETOLOAD].clear();
			return 0;
		}
		else {
			ParameterMap[PARAMETERMAP_CURRENTPROFILENAME] = profilenametoload;
		}
#ifdef LASTFM_DEBUG
	LOGDBG("cPluginLastfm::MainMenuAction: %s\r\n", ParameterMap[PARAMETERMAP_CURRENTPROFILENAME].c_str());
#endif
	}


#ifdef LASTFM_DEBUG
	LOGDBG("AudioPlayer started\r\n");
#endif
	cControl::Launch(new cAudioControl());

	// will execut kOk, when function returns:
	if (ParameterMap[PARAMETERMAP_OSD_AUTOSTARTMODE].compare("1") == 0) {
		cRemote::Put(kOk, 1);
	}

	return NULL;
}


cMenuSetupPage * cPluginLastfm::SetupMenu(void)
{
	// Return a setup menu in case the plugin supports one.
	return new cSetupLastfm();
}


bool cPluginLastfm::SetupParse(const char * Name, const char * Value)
{
	// Parse your own setup parameters and store their values.
	return false;
}


bool cPluginLastfm::Service(const char * Id, void * Data)
{
	// Handle custom service requests from other plugins

	struct SubmissionHandle_v1_1 * Handle =
		(struct SubmissionHandle_v1_1 *)Data;

	if (strcmp(Id, LASTFM_SUBMISSION_HANDLE_v1_1) == 0) {

		if (Data) {
			Handle->Handshake = &cWebServiceConnectorWebservices_auth::getMobileSession;
			Handle->Submission = &cWebServiceConnectorWebservices_track::scrobble;
		}

		return true;
	}

	return false;
}


const char * * cPluginLastfm::SVDRPHelpPages(void)
{
	// Return help text for SVDRP commands this plugin implements
	return NULL;
}


cString cPluginLastfm::SVDRPCommand(const char * Command, const char * Option, int & ReplyCode)
{
	// Process SVDRP commands this plugin implements
	return NULL;
}


VDRPLUGINCREATOR(cPluginLastfm); // Don't touch this!
