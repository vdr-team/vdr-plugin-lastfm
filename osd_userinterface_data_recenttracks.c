/**
 * osd_userinterface_data_recenttracks.c: Presents metadata
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-16 12:52:19  mf $
 */

#include "osd_userinterface_data_recenttracks.h"
#include <string>
#include <map> // MetadataMap
#include "osd_userinterface_data_artistMetadata.h"
#include "web_serviceconnector_webservices.h"
#include "config.h" // API_KEY
#include "logdefs.h"
#include <vdr/tools.h>
#include "utils.h" // html2text
#include <inttypes.h>
#include "ext/xmlParser/xmlParser.h"
#include <sstream>

extern std::map<std::string, std::string> ParameterMap; // username

cUserInterface_Data_recentTracks::cUserInterface_Data_recentTracks(void)
:cOsdMenu( tr("recenttracks"), 20)
{
	BuildItems();  // OSD-Elemente aufbauen
}


void cUserInterface_Data_recentTracks::BuildItems(void)
{
	std::string user(ParameterMap[PARAMETERMAP_USERNAME]);
	std::string api_key(APIKEY);
	std::string content;
	uint16_t limit=20;
	std::stringstream sstream_tmp;

	if (!cWebServiceConnectorWebservices::user.getRecentTracks(content, user, api_key, limit)) {
		/** parsing received xml data: */
	XMLNode xMainNode, xNode;
	XMLResults xe;
	xMainNode = XMLNode::parseString(content.c_str(), "lfm", &xe);

  if (xe.error)
     return;

	xNode = xMainNode.getChildNode("recenttracks");

  int n = xNode.nChildNode("track");

	for (int i=0; i<n; i++)
	{
		sstream_tmp.str("");
		sstream_tmp.clear();

		sstream_tmp << xNode.getChildNode("track",i).getChildNode("artist").getText();
		sstream_tmp << " - ";
		sstream_tmp << xNode.getChildNode("track",i).getChildNode("name").getText();


	      Add(new cOsdItem(hk(sstream_tmp.str().c_str())));
	}
	}
	Display();
}

