/*
 * audio_buffer.h: The audio buffer implementation
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: audio_buffer.h 256 2009-03-31 22:59:57Z hitman_47 $
 */
/*
 * audio_buffer.h: The audio buffer interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: audio_buffer.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef __AUDIOBUFFER_H
#define __AUDIOBUFFER_H

#include <stdint.h>
#include <vdr/thread.h>

class cSimpleRingBuffer {
private:
	uint8_t * mp_buffer;
	uint8_t * m_readPtr;
	uint8_t * m_writePtr;
	unsigned long m_bufferFree;
	unsigned long m_bufferSize;
	cMutex mo_BufferMutex;
	void Initialize(void);
public:
	cSimpleRingBuffer(unsigned long size = 0);
	~cSimpleRingBuffer();
	unsigned long Put(const uint8_t * data, unsigned long count);
	unsigned long Get(uint8_t * data, unsigned long count);
	unsigned long Available(void) const
	{
		return m_bufferSize - m_bufferFree;
	};
	unsigned long Free(void) const
	{
		return m_bufferFree;
	};
	unsigned long Size(void) const
	{
		return m_bufferSize;
	};
	void Clear(void);
};

#endif
