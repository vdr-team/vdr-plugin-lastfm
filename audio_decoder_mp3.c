/*
 * audio_decoder_mp3.c: The mp3 decoder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-05 22:08:44  mf $
 */

#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <vdr/skins.h>  // for OSD error messages
#include "audio_decoder_mp3.h"
#include "config.h"
#include <sstream>  // MyUserAgent()
#include <vdr/tools.h>
#include "logdefs.h"

cMp3Decoder::cMp3Decoder(void)
{
	mad_stream_init(&madStream);
	mad_frame_init(&madFrame);
	mad_synth_init(&madSynth);

	timeIndex = mad_timer_zero;
	m_sourcedataavailable = 0;
}


cMp3Decoder::~cMp3Decoder()
{
	m_sourcedataavailable = 0;
	mad_stream_finish(&madStream);
	mad_frame_finish(&madFrame);
	mad_synth_finish(&madSynth);
}


/// Functions copied from the vdr-PODCATCHER plugin:
int cMp3Decoder::FillReadBuffer(void)
{
	int unprocessedBytesInBuffer = 0;
	int bytesToRead = 0;
	int bytesRead = 0;

	if (madStream.next_frame != NULL) {
		unprocessedBytesInBuffer = madStream.bufend - madStream.next_frame;
		memmove(readBuffer, madStream.next_frame, unprocessedBytesInBuffer);
	} else {
		unprocessedBytesInBuffer = 0;
	}

	bytesToRead = readBufferSize - unprocessedBytesInBuffer;
	// Read data pieces from source:
	bytesRead = GetDataFromSource((readBuffer + unprocessedBytesInBuffer), bytesToRead);

	if (bytesRead == 0)
		m_sourcedataavailable = 0;
	else m_sourcedataavailable = 1;

#ifdef LASTFM_DEBUG_AUDIO
	LOGDBG("cMp3Decoder::FillReadBuffer:unprocessedBytesInBuffer: %d\r\n", unprocessedBytesInBuffer);
	LOGDBG("cMp3Decoder::FillReadBuffer:bytesRead: %d\r\n", bytesRead);
#endif

	return bytesRead + unprocessedBytesInBuffer;
}


void cMp3Decoder::DecodeNextChunk(void)
{
	int decodeResult = 1;

	do {
		// readBuffer wird hier gefuellt.
		int bytesInBuffer = FillReadBuffer();

		if (bytesInBuffer > 0) {
			mad_stream_buffer(&madStream, readBuffer, bytesInBuffer);
			// decodes the next frame's header and subband samples
			decodeResult = mad_frame_decode(&madFrame, &madStream);
		} else {
			break;
		}

	} while (decodeResult && MAD_RECOVERABLE(madStream.error));

	if (decodeResult == 0) {
		// synthesizes PCM samples from subband samples
		mad_synth_frame(&madSynth, &madFrame);
		mad_timer_add(&timeIndex, madFrame.header.duration);
	}

	if (decodeResult == -1) {
		// Following code suppresses stuttering like a "jumping cd" if no data is present:
		// synthesizes PCM samples from subband samples
		mad_synth_frame(&madSynth, &madFrame);
		mad_timer_add(&timeIndex, madFrame.header.duration);
		mad_frame_mute(&madFrame);
	}
}

//:PODCATCHER


mad_pcm *cMp3Decoder::GetData(void)
{
	DecodeNextChunk();

	return &madSynth.pcm;
}


int cMp3Decoder::GetIndex(int & Current, int & Total, bool SnapToIFrame)
{
	Current = 0;
	Total = 0;
	Current = timeIndex.seconds;
	////	Total = cPlaylist< cMetaDataPlaylist_Container >::getSingleton().begin( )->duration;

	return 0;
}

uint8_t cMp3Decoder::Status(void) {
	return m_sourcedataavailable;
}

