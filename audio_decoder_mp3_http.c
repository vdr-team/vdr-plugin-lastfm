/*
 * audio_decoder_mp3_http.c: The mp3 decoder via http
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id:  2009-07-15 17:50:33  mf $
 */

#include <sstream>
#include <vdr/tools.h>
#include "logdefs.h"
#include "audio_decoder_mp3_http.h"


cHttpMp3DecoderSocket::cHttpMp3DecoderSocket(ISocketHandler & h)
: HttpGetSocket(h)
{
	mo_NETBuffer = new cSimpleRingBuffer(NET_BUFFER_SIZE);
	SetDeleteByHandler(0);  /** Socket on stack will be destroyed after going out of scope. */
}


cHttpMp3DecoderSocket::~cHttpMp3DecoderSocket()
{
	if (mo_NETBuffer)
		DELETENULL(mo_NETBuffer);
}


void cHttpMp3DecoderSocket::OnHeader(const std::string& key,const std::string& value)
{
	/** Temporary redirect: */
	if (GetStatus().find("302")!= std::string::npos) {
		if (key.compare("Location")==0) {
			SetUrl(value);
			std::string host("");
			port_t port = 0;
			Url(value, host, port);

			Open(host,port);
			Handler().Add(this);
			Handler().Select();
		}
	}

	HttpGetSocket::OnHeader(key,value);
}


uint32_t cHttpMp3DecoderSocket::GetData(uint8_t * data, const uint32_t count)
{
	return mo_NETBuffer->Get(data, count);
}


void cHttpMp3DecoderSocket::OnRead(void)
{
	int32_t n = 0;
	unsigned char readBuffer[NET_READBUFFSIZE];
	unsigned char * readBuffer_Ptr;

	readBuffer_Ptr = readBuffer;
#ifdef LASTFM_DEBUG_AUDIO_HTTP
	LOGDBG("cHttpMp3Decoder::OnRead: mo_NETBuffer->Available: %ld\r\n", mo_NETBuffer->Available());
	LOGDBG("cHttpMp3Decoder::OnRead: mo_NETBuffer->Free: %ld\r\n", mo_NETBuffer->Free());
#endif
	if (mo_NETBuffer->Free() > NET_READBUFFSIZE) {
		n = recv(GetSocket(), readBuffer_Ptr, NET_READBUFFSIZE, MSG_NOSIGNAL);

		if (n > 0)
			mo_NETBuffer->Put((unsigned char *)readBuffer_Ptr, n);
	}

	HttpGetSocket::OnRead((char*)readBuffer_Ptr, n);
}


std::string cHttpMp3DecoderSocket::MyUseragent(void)
{
	std::stringstream buff_stream;

	buff_stream << PLUGIN_NAME << "/" << PLUGIN_VERSION;

	return buff_stream.str();
}


cHttpMp3Decoder::cHttpMp3Decoder(void)
:p(h)
,m_active(0)
{
	m_active = 0;
}


cHttpMp3Decoder::~cHttpMp3Decoder()
{
	m_active = 0;
	Cancel(MAXIMUM_THREADKILLTIME);
}


uint8_t cHttpMp3Decoder::Open(std::string url)
{
	std::string host("");
	port_t port = 0;

	p.SetUrl(url);
	p.Url(url, host, port);
	p.Open(host, port);

	h.Add(&p);
	h.Select(3,0); /** Wait until connection has been established, so set IsConnected() to true. */

	Start();

	return 0;
}


uint32_t cHttpMp3Decoder::GetDataFromSource(uint8_t * data, const uint32_t count)
{
	return p.GetData(data,count);
}


void cHttpMp3Decoder::Action(void)
{
	m_active = 1;
	while (Running() && m_active) {
		h.Select(1,0);
		cCondWait::SleepMs(3);
	}
	m_active = 0;
}


uint8_t cHttpMp3Decoder::IsConnected(void)
{
	return p.IsConnected();
}

