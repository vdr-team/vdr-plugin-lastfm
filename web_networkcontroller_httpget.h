/**
 * web_networkcontroller_httpget.h: HTTPGet
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: web_networkcontroller_httpget.h 256 2009-03-31 22:59:57Z hitman_47 $
 */

#ifndef WEB_NETWORKCONTROLLER_HTTPGET_H_
#define WEB_NETWORKCONTROLLER_HTTPGET_H_

#include <string.h>
#include <stdlib.h>
#include <Sockets/HttpGetSocket.h>

class My_Web_HttpGetSocket : public HttpGetSocket
{
public:
	My_Web_HttpGetSocket(ISocketHandler &, const std::string & url,
			     const std::string & to_file = "");
private:
	virtual void OnConnect(void);
};

#endif // WEB_NETWORKCONTROLLER_HTTPGET_H_
